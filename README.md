# Common Warehouse J2020 Project

Run service:
1. Run with idea help 
1.1 Edit configuration Runner (Add VM option ``-Dsping.profiles.active=local``)

2. Run with maven
 mvn spring-boot:run -Dspring-boot.run.jvmArguments="-Dspring.profiles.active=local"

3. Jar
3.1. mvn clean package
3.2 java -jar -Dspring.profiles.active=local /target/warehouse-1....

4. Docker 
4.1 mvn clean package
4.2.1 Windows - docker build -f windows/Dockerfile -t warehouse:${tag.name} .
4.2.2 Linux  - docker build -f linux/Dockerfile -t warehouse:${tag.name} . 
4.3 docker images - посмотреть все имеющиеся имеджи и найти id для warehouse = ${warehouse.image.id}
4.4 docker run -d -p ${port}:8080 ${warehouse.image.id}
4.5. Если пишет - не могу подключиться к БД - проверьте, поднят ли postgresql в docker
4.6. docker ps - посмотреть все поднятые контейнеры в докере
4.7. docker ps -a - посмотреть все контейнеры, которые когда-либо  поднимались в докере
4.8. Находим container id для контейнера с postgresql = ${postgres.container.id}
4.9. docker start ${postgres.container.id}
