package by.shag.warehouse.exception;

public class EntityDuplicationException extends RuntimeException {

    public EntityDuplicationException(String message) {
        super(message);
    }
}
