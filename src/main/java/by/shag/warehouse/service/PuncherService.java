package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.api.dto.PuncherSearchCriteriaDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Puncher;
import by.shag.warehouse.jpa.repository.PuncherRepository;
import by.shag.warehouse.mapping.PuncherMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PuncherService {

    @Autowired
    private PuncherRepository repository;
    @Autowired
    private PuncherMapper mapper;

    public PuncherDto save(PuncherDto puncherDto) {
        validateAlreadyExists(puncherDto, null);
        Puncher entity = mapper.mapFromDto(puncherDto);
        Puncher saved = repository.save(entity);
        return mapper.mapFromModel(saved);
    }

    public PuncherDto findById(Integer id) {
        Optional<Puncher> optionalPuncher = repository.findById(id);
        Puncher entity = optionalPuncher.orElseThrow(
                () -> new EntityNotFoundException("Model(Punher) with this ID not found"));
        return mapper.mapFromModel(entity);
    }

    public List<PuncherDto> findAll(PuncherSearchCriteriaDto puncherSearchCriteriaDto) {
        List<Puncher> entityList = repository.findAll(puncherSearchCriteriaDto);
        return mapper.mapListFromModel(entityList);
    }

    public PuncherDto update(Integer id, PuncherDto puncherDto) {
        validateAlreadyExists(puncherDto, id);
        Puncher entity = mapper.mapFromDto(puncherDto);
        entity.setId(id);
        Puncher updated = repository.update(entity);
        return mapper.mapFromModel(updated);
    }

    public void deleteById(Integer id) {
        PuncherDto dto = findById(id);
        repository.deleteById(dto.getId());
    }

    private void validateAlreadyExists(PuncherDto dto, Integer id) {
        Optional<Puncher> entity = repository.findByNameAndProducer(dto.getName(), dto.getProducerId());
        if (entity.isPresent() && !entity.get().getId().equals(id)) {
            throw new EntityDuplicationException("Name - Producer: " +
                    dto.getName() + "-" +
                    dto.getId() +
                    " -- is not unique (product Puncher)");
        }
    }

    public Double findAveragePriceInCents() {
        Double result = repository.getAveragePrice();
        return Math.round(result * 100.0) / 100.0;
    }

    public Integer findMinimumPriceInCents() {
        return repository.getMinPrice();
    }

    public Long CountAllRecordsInClassPuncher() {
        return repository.getCountPuncher();
    }
}
