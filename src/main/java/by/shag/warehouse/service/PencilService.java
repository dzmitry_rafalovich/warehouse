package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.BasketPencilDto;
import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import by.shag.warehouse.jpa.repository.PencilRepository;
import by.shag.warehouse.mapping.PencilMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.Instant;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional
public class PencilService {

    private final PencilRepository repository;
    private final PencilMapper mapper;

    public List<PencilDto> findByPriceInCentsBetween(Integer priceInCentsMin, Integer priceInCentsMax) {
        List<Pencil> modelList = repository.findByPriceInCentsBetweenOrderByIdDesc(priceInCentsMin, priceInCentsMax);
        return mapper.mapListFromModel(modelList);
    }

    public List<PencilDto> findByHardnessAndEraser(PencilHardness hardness, Boolean eraser) {
        List<Pencil> modelList = repository.findByHardnessAndEraser(hardness, eraser);
        return mapper.mapListFromModel(modelList);
    }

    public List<PencilDto> findByNameStartingWithIgnoreCase(String name) {
        List<Pencil> modelList = repository.findByNameStartingWithIgnoreCase(name);
        return mapper.mapListFromModel(modelList);
    }

    public List<PencilDto> findByProductionDateBefore(Instant productionDate) {
        List<Pencil> modelList = repository.findByProductionDateBefore(productionDate);
        return mapper.mapListFromModel(modelList);
    }

    public List<PencilDto> findByCountryNameContainingOrProducerNameContaining(String countryName, String producerName) {
        List<Pencil> modelList = repository.findByCountryNameContainingOrProducerNameContaining(countryName, producerName);
        return mapper.mapListFromModel(modelList);
    }

    public PencilDto save(PencilDto entity) {
        validateAlreadyExists(null, entity);
        Pencil model = mapper.mapFromDto(entity);
        model = repository.save(model);
        return mapper.mapFromModel(model);
    }

    public PencilDto update(Integer id, PencilDto entity) {
        validateAlreadyExists(id, entity);
        Pencil model = mapper.mapFromDto(entity);
        model.setId(id);
        model = repository.save(model);
        return mapper.mapFromModel(model);
    }

    public void deleteById(Integer id) {
        if (repository.findById(id).isPresent()) {
            repository.deleteById(id);
        } else {
            throw new EntityNotFoundException("Pencil with ID = " + id + " not found (delete section)");
        }
    }

    public PencilDto findById(Integer id) {
        Optional<Pencil> model = repository.findById(id);
        return mapper.mapFromModel(model.orElseThrow(() -> new EntityNotFoundException("Pencil with ID = " + id + " not found")));
    }

    public List<PencilDto> findAll(PencilSearchCriteriaDto search) {
        List<Pencil> modelList = repository.findAllWithSearchCriteriaDto(search);
        return mapper.mapListFromModel(modelList);
    }

    private void validateAlreadyExists(Integer id, PencilDto dto) {
        Optional<Pencil> check = repository.findByNameAndProducerId(dto.getName(), dto.getProducerId());
        if (check.isPresent() && !Objects.equals(check.get().getId(), id)) {
            throw new EntityDuplicationException("Name(" + dto.getName() + ") & producer ID(" + dto.getProducerId() + ") is not unique (product Pencil)");
        }
    }

    public Integer calculateTheBasket(BasketPencilDto request) {
        return request.getPencilDtoList().stream()
                .mapToInt(item -> {
                    Integer price = repository.findById(item.getId()).orElse(new Pencil().setPriceInCents(0)).getPriceInCents();
                    return price * item.getQuantity();
                }).sum();
    }

}
