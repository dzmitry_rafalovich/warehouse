package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.mapping.CountryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class CountryService {

    @Autowired
    private CountryRepository repository;
    @Autowired
    private CountryMapper mapper;

    public CountryDto save(CountryDto countryDto) {
        validateAlreadyExists(countryDto, null);
        Country entity = mapper.mapFromDto(countryDto);
        Country saved = repository.save(entity);
        return mapper.mapFromModel(saved);
    }

    public CountryDto findById(Integer id) {
        Optional<Country> optionalCountry = repository.findById(id);
        Country country = optionalCountry.orElseThrow(
                () -> new EntityNotFoundException("Country with ID = " + id + " not found"));
        return mapper.mapFromModel(country);
    }

    public Country findByIdWhichWillReturnModel(Integer id) {
        Optional<Country> optionalCountry = repository.findById(id);
        return optionalCountry.orElseThrow(
                () -> new EntityNotFoundException("Country with ID = " + id + " not found"));
    }

    public List<CountryDto> findAll() {
        List<Country> countryList = repository.findAll();
        return mapper.mapListFromModel(countryList);
    }

    public CountryDto update(Integer id, CountryDto countryDto) {
        validateAlreadyExists(countryDto, id);
        Country country = mapper.mapFromDto(countryDto);
        country.setId(id);
        Country updated = repository.save(country);
        return mapper.mapFromModel(updated);
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    private void validateAlreadyExists(CountryDto countryDto, Integer id) {
        Optional<Country> country = repository.findByName(countryDto.getName());
        if (country.isPresent() && !country.get().getId().equals(id)) {
            throw new EntityDuplicationException("Country name is not unique");
        }
    }
}
