package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.GenaBukinSocksCriteriaDto;
import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import by.shag.warehouse.jpa.repository.GenaBukinSocksRepository;
import by.shag.warehouse.mapping.GenaBukinSocksMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class GenaBukinSocksService {

    @Autowired
    private GenaBukinSocksRepository repository;
    @Autowired
    private GenaBukinSocksMapper mapper;

    public GenaBukinSocksDto save(GenaBukinSocksDto genaBukinSocksDto) {
        validateAlreadyExists(genaBukinSocksDto, null);
        GenaBukinSocks entity = mapper.mapFromDto(genaBukinSocksDto);
        GenaBukinSocks saved = repository.save(entity);
        return mapper.mapFromModel(saved);
    }

    public GenaBukinSocksDto findById(Integer id) {
        Optional<GenaBukinSocks> optionalGenaBukinSocks = repository.findById(id);
        GenaBukinSocks genaBukinSocks = optionalGenaBukinSocks.orElseThrow(
                () -> new EntityNotFoundException("GenaBukinSocks with this ID not found"));
        return mapper.mapFromModel(genaBukinSocks);
    }

    public List<GenaBukinSocksDto> findAll(GenaBukinSocksCriteriaDto criteriaDto) {
        List<GenaBukinSocks> genaBukinSocksList = repository.findAll(criteriaDto);
        return mapper.mapListFromModel(genaBukinSocksList);
    }

    public Double getAvrHolesQuantity() {
        return repository.getAvrHolesQuantity();
    }

    public GenaBukinSocksDto update(GenaBukinSocksDto genaBukinSocksDto, Integer id) {
        validateAlreadyExists(genaBukinSocksDto, id);
        GenaBukinSocks genaBukinSocks = mapper.mapFromDto(genaBukinSocksDto);
        genaBukinSocks.setId(id);
        GenaBukinSocks updated = repository.update(genaBukinSocks);
        return mapper.mapFromModel(updated);
    }

    public void deleteById(Integer id) {
        GenaBukinSocks model = repository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Socks not found"));
        repository.deleteById(model.getId());
    }

    private void validateAlreadyExists(GenaBukinSocksDto dto, Integer id) {
        Optional<GenaBukinSocks> genaBukinSocks = repository.findByName(dto.getName(), dto.getProducerId());
        if (genaBukinSocks.isPresent() && !genaBukinSocks.get().getId().equals(id)) {
            throw new EntityDuplicationException("Socks name is not unique");
        }
    }
}