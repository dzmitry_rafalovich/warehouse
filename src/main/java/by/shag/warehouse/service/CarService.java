package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Car;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.repository.CarRepository;
import by.shag.warehouse.mapping.CarMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
@Transactional
public class CarService {

    @Autowired
    private CarRepository repository;

    @Autowired
    private CarMapper mapper;

    public CarDto save(CarDto dto) {
        validateAlreadyExists(null, dto);
        Car entity = mapper.mapFromDto(dto);
        Car saved = repository.save(entity);
        return mapper.mapFromModel(saved);
    }

    public CarDto findById(Integer id) {
        Optional<Car> optionalCar = repository.findById(id);
        Car Car = optionalCar.orElseThrow(
                () -> new EntityServiceException("Car with this ID not found"));
        return mapper.mapFromModel(Car);

    }

    public List<CarDto> findAll() {
        List<Car> CarList = repository.findAll();
        return mapper.mapListFromModel(CarList);
    }

    public CarDto update(Integer id, CarDto dto) {
        validateAlreadyExists(id, dto);
        Car car = mapper.mapFromDto(dto);
        car.setId(id);
        Car updated = repository.update(car);
        return mapper.mapFromModel(updated);
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    private void validateAlreadyExists(Integer id, CarDto dto) {
        Optional<Pencil> check = repository.findByName(dto.getName(), dto.getProducerId());
        if (check.isPresent() && !Objects.equals(check.get().getId(), id)) {
            throw new EntityDuplicationException("Name(" + dto.getName() + ") & producer ID(" + dto.getProducerId()
                    + ") is not unique (product Pencil)");
        }
    }
}
