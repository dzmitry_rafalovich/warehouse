package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import by.shag.warehouse.mapping.ProducerMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProducerService {

    @Autowired
    private ProducerRepository repository;
    @Autowired
    private ProducerMapper mapper;

    public ProducerDto save(ProducerDto producerDto) {
        validateAlreadyExists(producerDto, null);
        Producer entity = mapper.mapFromDto(producerDto);
        Producer saved = repository.save(entity);
        return mapper.mapFromModel(saved);
    }

    public ProducerDto findById(Integer id) {
        Optional<Producer> optionalProducer = repository.findById(id);
        Producer producer = optionalProducer.orElseThrow(
                () -> new EntityNotFoundException("Producer with ID = " + id + " not found"));
        return mapper.mapFromModel(producer);
    }

    public Producer findByIdWhichWillReturnModel(Integer id) {
        Optional<Producer> optionalProducer = repository.findById(id);
        return optionalProducer.orElseThrow(
                () -> new EntityNotFoundException("Producer with ID = " + id + " not found"));
    }

    public List<ProducerDto> findAll() {
        List<Producer> producerList = repository.findAll();
        return mapper.mapListFromModel(producerList);
    }

    public ProducerDto update(Integer id, ProducerDto producerDto) {
        validateAlreadyExists(producerDto, id);
        Producer producer = mapper.mapFromDto(producerDto);
        producer.setId(id);
        Producer updated = repository.save(producer);
        return mapper.mapFromModel(updated);
    }

    public void deleteById(Integer id) {
        repository.deleteById(id);
    }

    private void validateAlreadyExists(ProducerDto producerDto, Integer id) {
        Optional<Producer> producer = repository.findByName(producerDto.getName());
        if (producer.isPresent() && !producer.get().getId().equals(id)) {
            throw new EntityDuplicationException("Producer name is not unique");
        }
    }
}
