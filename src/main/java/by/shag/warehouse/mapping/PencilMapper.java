package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.jpa.model.Pencil;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class PencilMapper extends AbstractItemMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "country", ignore = true)
    @Mapping(target = "producer", ignore = true)
    public abstract Pencil mapFromDto(PencilDto modelDto);

    @Mapping(target = "countryId", source = "country.id")
    @Mapping(target = "producerId", source = "producer.id")
    public abstract PencilDto mapFromModel(Pencil model);

    public abstract List<PencilDto> mapListFromModel(List<Pencil> modelList);

    public abstract List<Pencil> mapListFromDto(List<PencilDto> modelDtoList);

}