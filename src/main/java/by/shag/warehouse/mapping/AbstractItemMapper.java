package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.AbstractItemDto;
import by.shag.warehouse.jpa.model.AbstractItem;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.service.CountryService;
import by.shag.warehouse.service.ProducerService;
import org.mapstruct.AfterMapping;
import org.mapstruct.MappingTarget;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class AbstractItemMapper {

    @Autowired
    private CountryService countryService;
    @Autowired
    private ProducerService producerService;

    @AfterMapping
    protected void setCountryAndProducer(@MappingTarget AbstractItem model, AbstractItemDto dto) {
        Country country = countryService.findByIdWhichWillReturnModel(dto.getCountryId());
        Producer producer = producerService.findByIdWhichWillReturnModel(dto.getProducerId());

        model.setCountry(country);
        model.setProducer(producer);
    }
}
