package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class GenaBukinSocksMapper extends AbstractItemMapper {

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "country", ignore = true)
    @Mapping(target = "producer", ignore = true)
    public abstract GenaBukinSocks mapFromDto(GenaBukinSocksDto dto);

    @Mapping(target = "countryId", source = "country.id")
    @Mapping(target = "producerId", source = "producer.id")
    public abstract GenaBukinSocksDto mapFromModel(GenaBukinSocks model);

    public abstract List<GenaBukinSocksDto> mapListFromModel(List<GenaBukinSocks> modelList);

    public abstract List<GenaBukinSocks> mapListFromDto(List<GenaBukinSocksDto> dtoList);

}