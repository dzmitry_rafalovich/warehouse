package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.Puncher;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class PuncherMapper extends AbstractItemMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "country", ignore = true)
    @Mapping(target = "producer", ignore = true)
    public abstract Puncher mapFromDto(PuncherDto modelDto);

    @Mapping(target = "countryId", source = "country.id")
    @Mapping(target = "producerId", source = "producer.id")
    public abstract PuncherDto mapFromModel(Puncher model);

    public abstract List<PuncherDto> mapListFromModel(List<Puncher> modelList);

    public abstract List<Puncher> mapListFromDto(List<PuncherDto> modelDtoList);
}
