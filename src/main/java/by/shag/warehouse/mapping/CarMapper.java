package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.jpa.model.Car;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public abstract class CarMapper extends AbstractItemMapper {
    @Mapping(target = "id", ignore = true)
    @Mapping(target = "country", ignore = true)
    @Mapping(target = "producer", ignore = true)
    public abstract Car mapFromDto(CarDto modelDto);

    @Mapping(target = "countryId", source = "country.id")
    @Mapping(target = "producerId", source = "producer.id")
    public abstract CarDto mapFromModel(Car model);

    public abstract List<CarDto> mapListFromModel(List<Car> modelList);

    public abstract List<Car> mapListFromDto(List<CarDto> modelDtoList);
}