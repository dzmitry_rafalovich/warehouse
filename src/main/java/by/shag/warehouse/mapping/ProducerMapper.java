package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.jpa.model.Producer;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface ProducerMapper {

    @Mapping(target = "id", ignore = true)
    Producer mapFromDto(ProducerDto modelDto);

    ProducerDto mapFromModel(Producer model);

    List<ProducerDto> mapListFromModel(List<Producer> modelList);

    List<Producer> mapListFromDto(List<ProducerDto> modelDtoList);

}