package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.jpa.model.Country;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(unmappedTargetPolicy = ReportingPolicy.ERROR)
public interface CountryMapper {

    @Mapping(target = "id", ignore = true)
    Country mapFromDto(CountryDto modelDto);

    CountryDto mapFromModel(Country model);

    List<CountryDto> mapListFromModel(List<Country> modelList);

    List<Country> mapListFromDto(List<CountryDto> modelDtoList);

}