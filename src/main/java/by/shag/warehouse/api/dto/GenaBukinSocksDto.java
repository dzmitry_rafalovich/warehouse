package by.shag.warehouse.api.dto;

import by.shag.warehouse.api.validation.NotFoundSuchCountry;
import by.shag.warehouse.api.validation.NotFoundSuchProducer;
import by.shag.warehouse.jpa.model.PowerSmell;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.Instant;

@Data
public class GenaBukinSocksDto implements AbstractItemDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;

    @ApiModelProperty(value = "Name", example = "name", required = true, position = 2)
    @NotBlank(message = "Name should be not null or empty")
    private String name;

    @ApiModelProperty(value = "Price in cents", example = "50", required = true, position = 3)
    @NotNull(message = "Price should be not null")
    @Min(value = 1, message = "Price should be more than 0")
    private Integer priceInCents;

    @ApiModelProperty(value = "Production date", example = "2021-09-09T00:00:00.0Z", required = true, position = 4)
    @NotNull(message = "Production date should be not null")
    @Past(message = "Production date cannot be later than current date and time")
    private Instant productionDate;

    @ApiModelProperty(value = "Guaranty in months", example = "1", required = true, position = 5)
    @NotNull(message = "Guaranty should be not null")
    @Min(value = 1, message = "Guaranty should be more than 0")
    private Integer guarantyInMonths;

    @ApiModelProperty(value = "ID country", example = "1", required = true, position = 6)
    @NotFoundSuchCountry
    private Integer countryId;

    @ApiModelProperty(value = "ID producer", example = "1", required = true, position = 7)
    @NotFoundSuchProducer
    private Integer producerId;

    @ApiModelProperty(value = "Holes quantity", example = "10", required = true, position = 8)
    @NotNull(message = "Holes quantity should be not null")
    private Integer holesQuantity;

    @ApiModelProperty(value = "Power smell (STRONGLY, WORST, DEADLY)", example = "DEADLY", required = true, position = 9)
    @NotNull(message = "Power smell should be not null")
    private PowerSmell powerSmell;

    @ApiModelProperty(value = "Fabric", example = "HB", required = true, position = 10)
    @NotNull(message = "Fabric should be not null")
    private String fabric;
}
