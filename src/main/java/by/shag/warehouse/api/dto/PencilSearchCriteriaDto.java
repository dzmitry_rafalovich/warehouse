package by.shag.warehouse.api.dto;

import by.shag.warehouse.jpa.model.PencilHardness;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PencilSearchCriteriaDto {

    private String name;

    private Integer minPrice;

    private Integer maxPrice;

    private String productionDate;

    private Integer countryId;

    private String countryName;

    private Integer producerId;

    private Boolean eraser;

    private PencilHardness hardness;

    private String color;

}
