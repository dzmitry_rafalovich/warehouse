package by.shag.warehouse.api.dto;

import by.shag.warehouse.api.validation.NotFoundSuchCountry;
import by.shag.warehouse.api.validation.NotFoundSuchProducer;
import by.shag.warehouse.jpa.model.PencilHardness;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.time.Instant;

@Data
public class PencilDto implements AbstractItemDto {

    @ApiModelProperty(value = "id", example = "1", position = 1)
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private Integer id;

    @ApiModelProperty(value = "Name", example = "Test", required = true, position = 2)
    @NotBlank(message = "Name must be not null and not empty")
    private String name;

    @ApiModelProperty(value = "Price in cents", example = "10", required = true, position = 3)
    @NotNull(message = "Price (in cents) must be not null")
    @Min(value = 1, message = "Price (in cents) must be more than 0")
    private Integer priceInCents;

    @ApiModelProperty(value = "Production date", example = "2021-08-31T00:00:00.0Z", required = true, position = 4)
    @NotNull(message = "Production date must be not null")
    @Past(message = "Production date cannot be later than current date and time")
    private Instant productionDate;

    @ApiModelProperty(value = "Guaranty in months", example = "12", required = true, position = 5)
    @NotNull(message = "Guaranty (in months) must be not null")
    @Min(value = 1, message = "Guaranty (in months) must be more than 0")
    private Integer guarantyInMonths;

    @ApiModelProperty(value = "ID country", example = "1", required = true, position = 6)
    @NotFoundSuchCountry
    private Integer countryId;

    @ApiModelProperty(value = "ID producer", example = "1", required = true, position = 7)
    @NotFoundSuchProducer
    private Integer producerId;

    @ApiModelProperty(value = "Eraser", example = "true", required = true, position = 8)
    @NotNull(message = "Eraser must be not null")
    private Boolean eraser;

    @ApiModelProperty(value = "Pencil hardness (can be on from: B, H, HB, F)", example = "H", required = true, position = 9)
    @NotNull(message = "Hardness must be not null")
    private PencilHardness hardness;

    @ApiModelProperty(value = "Color", example = "black", required = true)
    @NotBlank(message = "Color must be not null and not empty")
    private String color;

}
