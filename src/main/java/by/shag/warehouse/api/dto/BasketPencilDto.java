package by.shag.warehouse.api.dto;

import lombok.Data;

import javax.validation.Valid;
import java.util.List;

@Data
public class BasketPencilDto {

    private List<@Valid ItemPencilDto> pencilDtoList;
}
