package by.shag.warehouse.api.dto;

import by.shag.warehouse.api.validation.NotFoundSuchPencil;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;

@Data
public class ItemPencilDto {

    @ApiModelProperty(value = "id", example = "1")
    @NotFoundSuchPencil
    private Integer id;

    @ApiModelProperty(value = "quantity", example = "1")
    @NotNull(message = "Quantity must be not null")
    @PositiveOrZero(message = "Quantity must be greater than or equal to 0")
    private Integer quantity;
}
