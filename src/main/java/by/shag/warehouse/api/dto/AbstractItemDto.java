package by.shag.warehouse.api.dto;

public interface AbstractItemDto {

    Integer getCountryId();
    Integer getProducerId();
}
