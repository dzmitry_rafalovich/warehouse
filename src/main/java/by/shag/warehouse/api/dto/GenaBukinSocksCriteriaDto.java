package by.shag.warehouse.api.dto;

import by.shag.warehouse.jpa.model.PowerSmell;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class GenaBukinSocksCriteriaDto {

    private String name;

    private Integer maxPrice;

    private Integer minPrice;

    private Integer guarantyInMonths;

    private Integer countryId;

    private Integer producerId;

    private Integer holesQuantity;

    private PowerSmell powerSmell;

    private String fabric;
}
