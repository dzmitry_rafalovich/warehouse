package by.shag.warehouse.api.dto;

import by.shag.warehouse.jpa.model.PuncherKnives;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PuncherSearchCriteriaDto {

    private Integer maxPrice;
    private Integer minPrice;
    private PuncherKnives puncherKnives;
    private Integer maxNumberOfSheets;
    private Integer minNumberOfSheets;
    private Integer countryId;
    private String countryName;
    private Integer producerId;
    private String puncherName;
    private String sortedBy;
    private Boolean discount;
}
