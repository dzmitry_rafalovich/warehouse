package by.shag.warehouse.api.dto;

import by.shag.warehouse.api.validation.NotFoundSuchCountry;
import by.shag.warehouse.api.validation.NotFoundSuchProducer;
import by.shag.warehouse.jpa.model.PuncherKnives;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.time.Instant;

@Data
public class PuncherDto implements AbstractItemDto {

    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    @ApiModelProperty(value = "ID", example = "1", position = 1)
    private Integer id;

    @NotBlank(message = "Name must be not null and not empty")
    @ApiModelProperty(value = "Name", example = "Дырокол для новичка0",
            required = true, position = 2)
    private String name;

    @NotNull(message = "Price (in cents) must be not null")
    @Min(value = 1, message = "Price (in cents) must be more than 0")
    @ApiModelProperty(value = "Price (in cents)", example = "150",
            required = true, position = 3)
    private Integer priceInCents;

    @NotNull(message = "Production date must be not null")
    @Past(message = "Production date cannot be later than current date and time")
    @ApiModelProperty(value = "Production date", example = "2021-01-01T00:00:00.0Z",
            required = true, position = 4)
    private Instant productionDate;

    @NotNull(message = "Guaranty (in months) must be not null")
    @Min(value = 1, message = "Guaranty (in months) must be more than 0")
    @ApiModelProperty(value = "Guaranty (in months)", example = "12",
            required = true, position = 5)
    private Integer guarantyInMonths;

    @ApiModelProperty(value = "ID (country)", example = "1",
            required = true, position = 6)
    @NotFoundSuchCountry
    private Integer countryId;

    @ApiModelProperty(value = "ID (producer)", example = "1",
            required = true, position = 7)
    @NotFoundSuchProducer
    private Integer producerId;

    @NotNull(message = "This number must be not null")
    @ApiModelProperty(value = "Number of knives {ONE, TWO, THREE}", example = "TWO",
            required = true, position = 8)
    private PuncherKnives puncherKnives;

    @NotNull(message = "This number must be not null")
    @Min(value = 10, message = "Number of Sheets starting from 10 and more")
    @ApiModelProperty(value = "Number of Sheets", example = "10",
            required = true, position = 9)
    private Integer maxSheets;

    @NotNull(message = "Discount must be not null")
    @ApiModelProperty(value = "Discount", example = "true",
            required = true, position = 10)
    private Boolean discount;
}
