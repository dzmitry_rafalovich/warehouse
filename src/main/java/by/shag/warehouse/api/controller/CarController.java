package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.service.CarService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/car")
@RequiredArgsConstructor
@Api(tags = "CRUD Car Controller")
public class CarController {

    private CarService service;

    @ApiOperation(value = "Find by ID")
    @GetMapping("/{id}")
    public CarDto findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @ApiOperation(value = "Find All")
    @GetMapping
    public List<CarDto> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Save")
    @PostMapping
    public CarDto save(@RequestBody @Valid CarDto dto) {
        return service.save(dto);
    }

    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public CarDto update(@PathVariable("id") Integer id,
                             @RequestBody @Valid CarDto dto) {
        return service.update(id, dto);
    }

    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id) {
        service.deleteById(id);
    }
}
