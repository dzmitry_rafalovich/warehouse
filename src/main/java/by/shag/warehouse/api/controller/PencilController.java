package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.BasketPencilDto;
import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.jpa.model.PencilHardness;
import by.shag.warehouse.service.PencilService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.Instant;
import java.util.List;

@RestController
@RequestMapping("/pencils")
@RequiredArgsConstructor
@Api(tags = "CRUD Pencil Controller")
public class PencilController {

    private final PencilService service;

    @ApiOperation(value = "Find all with prices in the specified range")
    @GetMapping("/search/priceInCents/{minPrice}/{maxPrice}")
    public List<PencilDto> findByPriceInCentsBetween(@PathVariable("minPrice") Integer minPrice,
                                                     @PathVariable("maxPrice") Integer maxPrice) {
        return service.findByPriceInCentsBetween(minPrice, maxPrice);
    }

    @ApiOperation(value = "Find all with specified hardness and eraser")
    @GetMapping("/search/hardnessAndEraser/{hardness}/{eraser}")
    public List<PencilDto> findByHardnessAndEraser(@PathVariable("hardness") PencilHardness hardness,
                                                   @PathVariable("eraser") Boolean eraser) {
        return service.findByHardnessAndEraser(hardness, eraser);
    }

    @ApiOperation(value = "Find all name start with specified(ignore case)")
    @GetMapping("/search/nameStartingWithIgnoreCase/{name}")
    public List<PencilDto> findByNameStartingWithIgnoreCase(@PathVariable("name") String name) {
        return service.findByNameStartingWithIgnoreCase(name);
    }

    @ApiOperation(value = "Find all with date before specified, ex. 2001-01-01T00:00:00Z")
    @GetMapping("/search/productionDateBefore/{productionDate}")
    public List<PencilDto> findByProductionDateBefore(@PathVariable("productionDate") Instant productionDate) {
        return service.findByProductionDateBefore(productionDate);
    }

    @ApiOperation(value = "Find all with specified country or producer names")
    @GetMapping("/search/countryNameContainingOrProducerNameContaining/{countryName}/{producerName}")
    public List<PencilDto> findByCountryNameContainingOrProducerNameContaining(@PathVariable(value = "countryName") String countryName,
                                                                               @PathVariable(value = "producerName") String producerName) {
        return service.findByCountryNameContainingOrProducerNameContaining(countryName, producerName);
    }

    @ApiOperation(value = "Find by ID")
    @GetMapping("/{id}")
    public PencilDto findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @ApiOperation(value = "Find All")
    @GetMapping
    public List<PencilDto> findAll(@RequestParam(required = false) String name,
                                   @RequestParam(value = "minPrice", required = false) Integer minPrice,
                                   @RequestParam(value = "maxPrice", required = false) Integer maxPrice,
                                   @RequestParam(value = "date, ex. 2001-01-01T00:00:00Z", required = false) String productionDate,
                                   @RequestParam(value = "country id", required = false) Integer countryId,
                                   @RequestParam(value = "country name", required = false) String countryName,
                                   @RequestParam(value = "producer", required = false) Integer producerId,
                                   @RequestParam(required = false) Boolean eraser,
                                   @RequestParam(required = false) PencilHardness hardness,
                                   @RequestParam(required = false) String color) {
        PencilSearchCriteriaDto search = new PencilSearchCriteriaDto(name, minPrice, maxPrice, productionDate, countryId, countryName, producerId, eraser, hardness, color);
        return service.findAll(search);
    }

    @ApiOperation(value = "Save")
    @PostMapping
    public PencilDto save(@RequestBody @Valid PencilDto dto) {
        return service.save(dto);
    }

    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public PencilDto update(@PathVariable("id") Integer id,
                            @RequestBody @Valid PencilDto dto) {
        return service.update(id, dto);
    }

    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Integer id) {
        service.deleteById(id);
    }

    @ApiOperation(value = "Basket, request for non-unique ID's")
    @PostMapping("/basket")
    public Integer calculateTheBasket(@RequestBody @Valid BasketPencilDto request) {
        return service.calculateTheBasket(request);
    }

}