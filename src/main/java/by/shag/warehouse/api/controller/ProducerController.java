package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.service.ProducerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/producers")
@Api(tags = "CRUD Producer Controller")
public class ProducerController {

    @Autowired
    private ProducerService service;

    @ApiOperation(value = "Find by ID")
    @GetMapping("/{id}")
    public ProducerDto findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @ApiOperation(value = "Find All")
    @GetMapping
    public List<ProducerDto> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Save")
    @PostMapping
    public ProducerDto save(@RequestBody @Valid ProducerDto dto) {
        return service.save(dto);
    }

    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public ProducerDto update(@PathVariable("id") Integer id,
                              @RequestBody @Valid ProducerDto dto) {
        return service.update(id, dto);
    }

    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id) {
        service.deleteById(id);
    }
}
