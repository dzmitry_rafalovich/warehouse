package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.service.CountryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/countries")
@Api(tags = "CRUD Country Controller")
public class CountryController {

    @Autowired
    private CountryService service;

    @ApiOperation(value = "Find by ID")
    @GetMapping("/{id}")
    public CountryDto findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @ApiOperation(value = "Find All")
    @GetMapping
    public List<CountryDto> findAll() {
        return service.findAll();
    }

    @ApiOperation(value = "Save")
    @PostMapping
    public CountryDto save(@RequestBody @Valid CountryDto dto) {
        return service.save(dto);
    }

    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public CountryDto update(@PathVariable("id") Integer id,
                             @RequestBody @Valid CountryDto dto) {
        return service.update(id, dto);
    }

    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Integer id) {
        service.deleteById(id);
    }
}