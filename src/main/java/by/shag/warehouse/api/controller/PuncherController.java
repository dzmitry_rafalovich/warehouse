package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.api.dto.PuncherSearchCriteriaDto;
import by.shag.warehouse.jpa.model.PuncherKnives;
import by.shag.warehouse.service.PuncherService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/punches")
@Api(tags = "CRUD Puncher Controller")
public class PuncherController {

    @Autowired
    PuncherService puncherService;

    @ApiOperation(value = "Find (ID) Puncher")
    @GetMapping("/{id}")
    public PuncherDto findById(@PathVariable Integer id) {
        return puncherService.findById(id);
    }

    @ApiOperation(value = "Average (price In Cents) Puncher")
    @GetMapping("/average-price")
    public Double findAverage() {
        return puncherService.findAveragePriceInCents();
    }

    @ApiOperation(value = "Min  of (price In Cents) Puncher")
    @GetMapping("/min-price")
    public Integer findMin() {
        return puncherService.findMinimumPriceInCents();
    }

    @ApiOperation(value = "Count records [Puncher]")
    @GetMapping("/count-puncher")
    public Long findCount() {
        return puncherService.CountAllRecordsInClassPuncher();
    }

    @ApiOperation(value = "Find (ALL) Puncher")
    @GetMapping
    public List<PuncherDto> findAll(
            @RequestParam(value = "maxPrice", required = false) Integer maxPrice,
            @RequestParam(value = "minPrice", required = false) Integer minPrice,
            @RequestParam(required = false) PuncherKnives puncherKnives,
            @RequestParam(value = "maxNumberOfSheets", required = false) Integer maxNumberOfSheets,
            @RequestParam(value = "minNumberOfSheets", required = false) Integer minNumberOfSheets,
            @RequestParam(required = false) Integer countryId,
            @RequestParam(value = "countryName", required = false) String countryName,
            @RequestParam(required = false) Integer producerId,
            @RequestParam(value = "puncherName", required = false) String puncherName,
            @RequestParam(required = false) String sortedBy,
            @RequestParam(required = false) Boolean discount

    ) {
        PuncherSearchCriteriaDto puncherSearchCriteriaDto =
                new PuncherSearchCriteriaDto(
                        maxPrice, minPrice, puncherKnives,
                        maxNumberOfSheets, minNumberOfSheets, countryId,
                        countryName, producerId,puncherName, sortedBy, discount);
        return puncherService.findAll(puncherSearchCriteriaDto);
    }

    @ApiOperation(value = "Create")
    @PostMapping
    public PuncherDto save(@RequestBody @Valid PuncherDto dto) {
        return puncherService.save(dto);
    }

    @ApiOperation(value = "Edit")
    @PutMapping("/{id}")
    public PuncherDto update(@PathVariable Integer id,
                             @RequestBody @Valid PuncherDto dto) {
        return puncherService.update(id, dto);
    }

    @ApiOperation(value = "Delete")
    @DeleteMapping("/{id}")
    public void deleteByID(@PathVariable Integer id) {
        puncherService.deleteById(id);
    }
}
