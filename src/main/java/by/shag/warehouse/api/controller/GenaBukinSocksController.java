package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.GenaBukinSocksCriteriaDto;
import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.jpa.model.PowerSmell;
import by.shag.warehouse.service.GenaBukinSocksService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/socks")
@Api(tags = "CRUD Socks Controller")
public class GenaBukinSocksController {

    @Autowired
    private GenaBukinSocksService service;

    @ApiOperation(value = "Find by ID")
    @GetMapping("/{id}")
    public GenaBukinSocksDto findById(@PathVariable("id") Integer id) {
        return service.findById(id);
    }

    @ApiOperation(value = "Get average of holes quantity")
    @GetMapping("/holesAvr")
    public Double getAvrHolesQuantity() {
        return service.getAvrHolesQuantity();
    }

    @ApiOperation(value = "Find All")
    @GetMapping
    public List<GenaBukinSocksDto> findAll(
            @RequestParam(required = false) String name,
            @RequestParam(value = "maxPrice", required = false) Integer maxPrice,
            @RequestParam(value = "minPrice", required = false) Integer minPrice,
            @RequestParam(required = false) Integer guarantyInMonths,
            @RequestParam(value = "country", required = false) Integer countryId,
            @RequestParam(value = "producer", required = false) Integer producerId,
            @RequestParam(required = false) Integer holesQuantity,
            @RequestParam(value = "smell", required = false) PowerSmell powerSmell,
            @RequestParam(required = false) String fabric
    ) {
        GenaBukinSocksCriteriaDto criteriaDto =
                new GenaBukinSocksCriteriaDto(name, maxPrice, minPrice, guarantyInMonths, countryId, producerId,
                        holesQuantity, powerSmell, fabric);
        return service.findAll(criteriaDto);
    }

    @ApiOperation(value = "Save")
    @PostMapping
    public GenaBukinSocksDto save(@RequestBody @Valid GenaBukinSocksDto dto) {
        return service.save(dto);
    }

    @ApiOperation(value = "Update")
    @PutMapping("/{id}")
    public GenaBukinSocksDto update(@RequestBody @Valid GenaBukinSocksDto dto,
                                    @PathVariable("id") Integer id) {
        return service.update(dto, id);
    }

    @ApiOperation(value = "Delete by ID")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Integer id) {
        service.deleteById(id);
    }

}
