package by.shag.warehouse.api.validation;

import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class NotFoundSuchCountryValidator implements ConstraintValidator<NotFoundSuchCountry, Integer> {

    @Autowired
    private CountryRepository repository;

    @Override
    public boolean isValid(Integer id, ConstraintValidatorContext constraintValidatorContext) {
        if (id == null) {
            return false;
        }
        Optional<Country> model = repository.findById(id);
        return model.isPresent();
    }
}