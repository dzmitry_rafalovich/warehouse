package by.shag.warehouse.api.validation;

import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.repository.PencilRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class NotFoundSuchPencilValidator implements ConstraintValidator<NotFoundSuchPencil, Integer> {

    @Autowired
    private PencilRepository repository;

    @Override
    public boolean isValid(Integer id, ConstraintValidatorContext constraintValidatorContext) {
        if (id == null) {
            return false;
        }
        Optional<Pencil> model = repository.findById(id);
        return model.isPresent();
    }
}