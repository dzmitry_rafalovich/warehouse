package by.shag.warehouse.api.validation;

import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import org.springframework.beans.factory.annotation.Autowired;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.Optional;

public class NotFoundSuchProducerValidator implements ConstraintValidator<NotFoundSuchProducer, Integer> {

    @Autowired
    private ProducerRepository repository;

    @Override
    public boolean isValid(Integer id, ConstraintValidatorContext constraintValidatorContext) {
        if (id == null) {
            return false;
        }
        Optional<Producer> model = repository.findById(id);
        return model.isPresent();
    }
}
