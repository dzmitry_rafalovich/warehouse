package by.shag.warehouse.api.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Constraint(validatedBy = NotFoundSuchPencilValidator.class)
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface NotFoundSuchPencil {

    String message() default "Pencil with this ID does not exist";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

}
