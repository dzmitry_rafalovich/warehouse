package by.shag.warehouse.jpa.model;

public enum PencilHardness {

    B, H, HB, F
    
}
