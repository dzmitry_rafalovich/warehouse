package by.shag.warehouse.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "Puncher")
@Data
public class Puncher implements AbstractItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(name = "price_in_cents",nullable = false)
    private Integer priceInCents;

    @Column(name = "production_date", nullable = false)
    private Instant productionDate;

    @Column(name = "guaranty_in_months", nullable = false)
    private Integer guarantyInMonths;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "producer_id")
    private Producer producer;

    @Column(name = "puncher_knives", nullable = false)
    @Enumerated(EnumType.STRING)
    private PuncherKnives puncherKnives;

    @Column(name = "max_sheets",nullable = false)
    private Integer maxSheets;

    @Column(name = "discount",nullable = false)
    private Boolean discount;
}
