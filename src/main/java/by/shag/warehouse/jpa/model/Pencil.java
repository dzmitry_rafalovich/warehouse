package by.shag.warehouse.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "Pencil")
@Data
public class Pencil implements AbstractItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(name = "price_in_cents", nullable = false)
    private Integer priceInCents;

    @Column(name = "production_date", nullable = false)
    private Instant productionDate;

    @Column(name = "guaranty_in_months", nullable = false)
    private Integer guarantyInMonths;

    @ManyToOne
    @JoinColumn(name = "country_id", nullable = false)
    private Country country;

    @ManyToOne
    @JoinColumn(name = "producer_id", nullable = false)
    private Producer producer;

    @Column(nullable = false)
    private Boolean eraser;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private PencilHardness hardness;

    @Column(nullable = false)
    private String color;

}
