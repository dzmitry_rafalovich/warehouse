package by.shag.warehouse.jpa.model;

import lombok.Data;

import javax.persistence.*;
import java.time.Instant;

@Entity
@Table(name = "Socks")
@Data
public class GenaBukinSocks implements AbstractItem {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

    @Column(name = "price_in_cents", nullable = false)
    private Integer priceInCents;

    @Column(name = "production_date", nullable = false)
    private Instant productionDate;

    @Column(name = "guaranty_in_months", nullable = false)
    private Integer guarantyInMonths;

    @ManyToOne
    @JoinColumn(name = "country_id")
    private Country country;

    @ManyToOne
    @JoinColumn(name = "producer_id")
    private Producer producer;

    @Column(name = "holes_quantity", nullable = false)
    private Integer holesQuantity;

    @Column(name = "power_smell", nullable = false)
    @Enumerated(EnumType.STRING)
    private PowerSmell powerSmell;

    @Column(nullable = false)
    private String fabric;

}
