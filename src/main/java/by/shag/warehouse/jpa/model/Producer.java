package by.shag.warehouse.jpa.model;

import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "Producer")
@Data
public class Producer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(nullable = false)
    private String name;

}
