package by.shag.warehouse.jpa.model;

import java.time.Instant;

public interface AbstractItem {

     Integer getId();

     String getName();

     Integer getPriceInCents();

     Instant getProductionDate();

     Integer getGuarantyInMonths();

     Country getCountry();

     Producer getProducer();

     AbstractItem setCountry(Country country);

     AbstractItem setProducer(Producer producer);
}
