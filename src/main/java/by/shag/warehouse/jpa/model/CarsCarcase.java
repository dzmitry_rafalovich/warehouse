package by.shag.warehouse.jpa.model;

public enum CarsCarcase {
    STATIONWAGON, SEDAN, HATCHBACK, JEEP
}
