package by.shag.warehouse.jpa.repository;

import java.util.List;
import java.util.Optional;

public interface CRUDRepository<E,ID>{

    E save(E entity);

    Optional<E> findById(ID id);

    List<E> findAll();

    E update(E entity);

    void deleteById(ID id);

}
