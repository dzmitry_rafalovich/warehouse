package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.PuncherSearchCriteriaDto;
import by.shag.warehouse.jpa.model.Puncher;
import by.shag.warehouse.jpa.model.PuncherKnives;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class PuncherRepository implements CRUDRepository<Puncher, Integer> {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Puncher save(Puncher entity) {
        entityManager.persist(entity);
        return entity;
    }

    @Override
    public Optional<Puncher> findById(Integer id) {
        Puncher entity = entityManager.find(Puncher.class, id);
        return Optional.ofNullable(entity);
    }

    @Override
    public List<Puncher> findAll() {
        Query query = entityManager.createQuery("SELECT n FROM Puncher n ORDER BY id", Puncher.class);
        List<Puncher> result = query.getResultList();
        return result;
    }

    @Override
    public Puncher update(Puncher entity) {
        entityManager.merge(entity);
        return entityManager.find(Puncher.class, entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
        Puncher entity = entityManager.find(Puncher.class, id);
        entityManager.remove(entity);
    }

    public Optional<Puncher> findByNameAndProducer(String name, Integer producerId) {
        Query query = entityManager.createQuery(
                "SELECT n FROM Puncher n WHERE n.name = : name AND producer_id = : producerId",
                Puncher.class)
                .setParameter("name", name)
                .setParameter("producerId", producerId);
        Object puncher = query.getResultStream()
                .findFirst()
                .orElse(null);
        return Optional.ofNullable((Puncher) puncher);
    }

    public List<Puncher> findAll(PuncherSearchCriteriaDto puncherSearchCriteriaDto) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Puncher> query = criteriaBuilder.createQuery(Puncher.class);
        Root<Puncher> root = query.from(Puncher.class);
        List<Predicate> predicates = new ArrayList<>();

        Integer maxPrice = puncherSearchCriteriaDto.getMaxPrice();
        if (maxPrice != null) {
            Predicate maxPricePredicate = criteriaBuilder
                    .lessThanOrEqualTo(root.get("priceInCents"), maxPrice);
            predicates.add(maxPricePredicate);
        }

        Integer minPrice = puncherSearchCriteriaDto.getMinPrice();
        if (minPrice != null) {
            Predicate minPricePredicate = criteriaBuilder
                    .greaterThanOrEqualTo(root.get("priceInCents"), minPrice);
            predicates.add(minPricePredicate);
        }

        Integer producerId = puncherSearchCriteriaDto.getProducerId();
        if (producerId != null) {
            Predicate producerPredicate = criteriaBuilder
                    .equal(root.get("producer").get("id"), producerId);
            predicates.add(producerPredicate);
        }

        Integer countryId = puncherSearchCriteriaDto.getCountryId();
        if (countryId != null) {
            Predicate countryPredicate = criteriaBuilder
                    .equal(root.get("country").get("id"), countryId);
            predicates.add(countryPredicate);
        }

        String countryName = puncherSearchCriteriaDto.getCountryName();
        if (countryName != null) {
            Predicate countryNamePredicate = criteriaBuilder
                    .equal(root.get("country")
                            .get("name"), countryName);
            predicates.add(countryNamePredicate);
        }

        Integer maxNumberOfSheets = puncherSearchCriteriaDto.getMaxNumberOfSheets();
        if (maxNumberOfSheets != null) {
            Predicate maxNumberOfSheetsPredicate = criteriaBuilder
                    .lessThanOrEqualTo(root.get("maxSheets"), maxNumberOfSheets);
            predicates.add(maxNumberOfSheetsPredicate);
        }

        Integer minNumberOfSheets = puncherSearchCriteriaDto.getMinNumberOfSheets();
        if (minNumberOfSheets != null) {
            Predicate minNumberOfSheetsPredicate = criteriaBuilder
                    .greaterThanOrEqualTo(root.get("maxSheets"), minNumberOfSheets);
            predicates.add(minNumberOfSheetsPredicate);
        }

        PuncherKnives puncherKnives = puncherSearchCriteriaDto.getPuncherKnives();
        if (puncherKnives != null) {
            Predicate puncherKnivesPredicate = criteriaBuilder
                    .equal(root.get("puncherKnives"), puncherKnives);
            predicates.add(puncherKnivesPredicate);
        }

        String puncherName = puncherSearchCriteriaDto.getPuncherName();
        if (puncherName != null) {
            Predicate puncherNamePredicate = criteriaBuilder
                    .like(root.get("name"), puncherName);
            predicates.add(puncherNamePredicate);
        }

        Boolean puncherDiscount = puncherSearchCriteriaDto.getDiscount();
        if (puncherDiscount != null) {
            Predicate puncherDiscountPredicate = criteriaBuilder
                    .equal(root.get("discount"), puncherDiscount);
            predicates.add(puncherDiscountPredicate);
        }

        String sortedBy = puncherSearchCriteriaDto.getSortedBy();
        if (sortedBy != null && (
                sortedBy.equals("priceInCents") ||
                        sortedBy.equals("country") ||
                        sortedBy.equals("producer") ||
                        sortedBy.equals("puncherKnives") ||
                        sortedBy.equals("discount")
        )) {
            query.orderBy(criteriaBuilder.asc(root.get(sortedBy)));
        } else {
            query.orderBy(criteriaBuilder.asc(root.get("id")));
        }

        query.select(root).where(predicates.toArray(new Predicate[predicates.size()]));
        return entityManager.createQuery(query).getResultList();
    }

    public Double getAveragePrice() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> query = criteriaBuilder.createQuery(Double.class);
        Root<Puncher> root = query.from(Puncher.class);
        query.select(criteriaBuilder.avg(root.get("priceInCents")));
        return entityManager.createQuery(query).getSingleResult();
    }

    public Integer getMinPrice() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
        Root<Puncher> root = query.from(Puncher.class);
        query.select(criteriaBuilder.min(root.get("priceInCents")));
        return entityManager.createQuery(query).getSingleResult();
    }

    public Long getCountPuncher() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<Puncher> root = query.from(Puncher.class);
        query.select(criteriaBuilder.count(root.get("id")));
        return entityManager.createQuery(query).getSingleResult();
    }
}