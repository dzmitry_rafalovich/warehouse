package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.GenaBukinSocksCriteriaDto;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import by.shag.warehouse.jpa.model.PowerSmell;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class GenaBukinSocksRepository implements CRUDRepository<GenaBukinSocks, Integer> {

    @PersistenceContext
    EntityManager entityManager;

    @Override
    public GenaBukinSocks save(GenaBukinSocks socks) {
        entityManager.persist(socks);
        return socks;
    }

    @Override
    public Optional<GenaBukinSocks> findById(Integer id) {
        GenaBukinSocks socks = entityManager.find(GenaBukinSocks.class, id);
        return Optional.ofNullable(socks);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<GenaBukinSocks> findAll() {
        Query query = entityManager.createQuery("SELECT s FROM GenaBukinSocks s ORDER BY id", GenaBukinSocks.class);
        return (List<GenaBukinSocks>) query.getResultList();
    }

    public List<GenaBukinSocks> findAll(GenaBukinSocksCriteriaDto criteriaDto) {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<GenaBukinSocks> query = criteriaBuilder.createQuery(GenaBukinSocks.class);
        Root<GenaBukinSocks> root = query.from(GenaBukinSocks.class);

        List<Predicate> predicateList = new ArrayList<>();
        String name = criteriaDto.getName();
        if (name != null) {
            Predicate namePredicate = criteriaBuilder.equal(root.get("name"), name);
            predicateList.add(namePredicate);
        }

        Integer maxPrice = criteriaDto.getMaxPrice();
        if (maxPrice != null) {
            Predicate maxPricePredicate = criteriaBuilder.lessThanOrEqualTo(root.get("priceInCents"), maxPrice);
            predicateList.add(maxPricePredicate);
        }

        Integer minPrice = criteriaDto.getMinPrice();
        if (minPrice != null) {
            Predicate minPricePredicate = criteriaBuilder.greaterThanOrEqualTo(root.get("priceInCents"), minPrice);
            predicateList.add(minPricePredicate);
        }

        Integer guaranty = criteriaDto.getGuarantyInMonths();
        if (guaranty != null) {
            Predicate guarantyPredicate = criteriaBuilder.equal(root.get("guarantyInMonths"), guaranty);
            predicateList.add(guarantyPredicate);
        }

        Integer countryId = criteriaDto.getCountryId();
        if (countryId != null) {
            Predicate countryPredicate = criteriaBuilder.equal(root.get("country").get("id"), countryId);
            predicateList.add(countryPredicate);
        }

        Integer producerId = criteriaDto.getProducerId();
        if (producerId != null) {
            Predicate producerPredicate = criteriaBuilder.equal(root.get("producer").get("id"), producerId);
            predicateList.add(producerPredicate);
        }

        Integer holesQuantity = criteriaDto.getHolesQuantity();
        if (holesQuantity != null) {
            Predicate holesQuantityPredicate = criteriaBuilder.equal(root.get("holesQuantity"), holesQuantity);
            predicateList.add(holesQuantityPredicate);
        }

        PowerSmell powerSmell = criteriaDto.getPowerSmell();
        if (powerSmell != null) {
            Predicate powerSmellPredicate = criteriaBuilder.equal(root.get("powerSmell"), powerSmell);
            predicateList.add(powerSmellPredicate);
        }

        String fabric = criteriaDto.getFabric();
        if (fabric != null) {
            Predicate fabricPredicate = criteriaBuilder.equal(root.get("fabric"), fabric);
            predicateList.add(fabricPredicate);
        }

        query.select(root).where(predicateList.toArray(new Predicate[predicateList.size()]));
        return entityManager.createQuery(query).getResultList();
    }

    public Double getAvrHolesQuantity() {
        CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
        CriteriaQuery<Double> query = criteriaBuilder.createQuery(Double.class);
        Root<GenaBukinSocks> root = query.from(GenaBukinSocks.class);
        query.select(criteriaBuilder.avg(root.get("holesQuantity")));
        return entityManager.createQuery(query).getSingleResult();
    }

    @Override
    public GenaBukinSocks update(GenaBukinSocks socks) {
        entityManager.merge(socks);
        return entityManager.find(GenaBukinSocks.class, socks.getId());
    }

    @Override
    public void deleteById(Integer id) {
        GenaBukinSocks socks = entityManager.find(GenaBukinSocks.class, id);
        entityManager.remove(socks);
    }

    public Optional<GenaBukinSocks> findByName(String name, Integer producerId) {
        Query query = entityManager.createQuery(
                        "SELECT c FROM GenaBukinSocks c where c.name = : name AND producer_id = : producerId", GenaBukinSocks.class)
                .setParameter("name", name)
                .setParameter("producerId", producerId);
        GenaBukinSocks genaBukinSocks = null;
        List result = query.getResultList();
        if (!result.isEmpty()) {
            genaBukinSocks = (GenaBukinSocks) result.get(0);
        }
        return Optional.ofNullable(genaBukinSocks);
    }
}
