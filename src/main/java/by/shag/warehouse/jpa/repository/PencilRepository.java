package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

public interface PencilRepository extends JpaRepository<Pencil, Integer> {

    List<Pencil> findByPriceInCentsBetweenOrderByIdDesc(Integer priceInCentsMin, Integer priceInCentsMax);

    List<Pencil> findByHardnessAndEraser(PencilHardness hardness, Boolean eraser);

    List<Pencil> findByNameStartingWithIgnoreCase(String name);

    List<Pencil> findByProductionDateBefore(Instant productionDate);

    List<Pencil> findByCountryNameContainingOrProducerNameContaining(String countryName, String producerName);

    Optional<Pencil> findByNameAndProducerId(String name, Integer producerId);

    default List<Pencil> findAllWithSearchCriteriaDto(PencilSearchCriteriaDto search) {
        throw new UnsupportedOperationException();
    }

}
