package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.jpa.model.Car;
import by.shag.warehouse.jpa.model.Pencil;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;
import java.util.Optional;

@Repository
public class CarRepository implements CRUDRepository<Car, Integer> {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Car save(Car entity) {
        em.persist(entity);
        return entity;
    }

    @Override
    public Optional<Car> findById(Integer id) {
        return Optional.ofNullable(em.find(Car.class, id));
    }


    @Override
    public List<Car> findAll() {
        return em
                .createQuery("SELECT n FROM Car n ORDER BY id", Car.class)
                .getResultList();
    }

    @Override
    public Car update(Car entity) {
        em.merge(entity);
        return em.find(Car.class, entity.getId());
    }

    @Override
    public void deleteById(Integer id) {
        em.remove(em.find(Car.class, id));
    }

    public Optional<Pencil> findByName(String name, Integer producerId) {
        Query query = em.createQuery("SELECT n FROM Car n WHERE name = : name AND producer_id = : producerId",
                        Car.class)
                .setParameter("name", name)
                .setParameter("producerId", producerId);
        boolean check = query.getResultStream()
                .findFirst()
                .isPresent();
        return check ? Optional.of((Pencil) query.getSingleResult()) : Optional.empty();
    }
}