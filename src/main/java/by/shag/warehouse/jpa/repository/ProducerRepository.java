package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.jpa.model.Producer;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface ProducerRepository extends JpaRepository<Producer, Integer> {

    Optional<Producer> findByName(String name);
}
