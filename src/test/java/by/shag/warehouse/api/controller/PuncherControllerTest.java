package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.model.PuncherKnives;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import by.shag.warehouse.service.PuncherService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PuncherController.class)
class PuncherControllerTest {

    @MockBean
    private CountryRepository countryRepository;
    @MockBean
    private ProducerRepository producerRepository;
    @MockBean
    private PuncherService service;

    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    private static final Integer ID = 1;
    private static final String NAME = "Puncher name";
    private static final Integer PRICE_IN_CENTS = 2;
    private static final Instant PRODUCTION_DATE = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 12;
    private static final Country COUNTRY = new Country().setId(1).setName("Country name");
    private static final Producer PRODUCER = new Producer().setId(1).setName("Producer name");
    private static final PuncherKnives PUNCHER_KNIVES = PuncherKnives.TWO;
    private static final Integer MAX_SHEETS = 10;
    private static final Boolean DISCOUNT = true;

    @Test
    void findById() throws Exception {
        when(service.findById(anyInt())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/punches" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.puncherKnives", is(PUNCHER_KNIVES.toString())))
                .andExpect(jsonPath("$.maxSheets", is(MAX_SHEETS)))
                .andExpect(jsonPath("$.discount", is(DISCOUNT)));
    }

    @Test
    void findAll() throws Exception {
        when(service.findAll(any())).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/punches"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].name", is(NAME)))
                .andExpect(jsonPath("$.[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[0].productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[0].puncherKnives", is(PUNCHER_KNIVES.toString())))
                .andExpect(jsonPath("$.[0].maxSheets", is(MAX_SHEETS)))
                .andExpect(jsonPath("$.[0].discount", is(DISCOUNT)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].name", is(NAME)))
                .andExpect(jsonPath("$.[1].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[1].productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.[1].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[1].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[1].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[1].puncherKnives", is(PUNCHER_KNIVES.toString())))
                .andExpect(jsonPath("$.[1].maxSheets", is(MAX_SHEETS)))
                .andExpect(jsonPath("$.[1].discount", is(DISCOUNT)));
    }

    @Test
    void save() throws Exception {
        when(service.save(any(PuncherDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.puncherKnives", is(PUNCHER_KNIVES.toString())))
                .andExpect(jsonPath("$.maxSheets", is(MAX_SHEETS)))
                .andExpect(jsonPath("$.discount", is(DISCOUNT)));
    }

    @Test
    void saveWithNullData() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(new PuncherDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", hasSize(9)))
                .andExpect(jsonPath("$.message", containsInAnyOrder("Guaranty (in months) must be not null",
                        "Country with this ID does not exist",
                        "Name must be not null and not empty",
                        "Production date must be not null",
                        "This number must be not null",
                        "Price (in cents) must be not null",
                        "This number must be not null",
                        "Producer with this ID does not exist",
                        "Discount must be not null")));
    }

    @Test
    void saveWithNoCorrectGuarantyInMonths() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setGuarantyInMonths(-1))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Guaranty (in months) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectCountryId() throws Exception {
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setCountryId(-1))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Country with this ID does not exist")));
    }

    @Test
    void saveWithEmptyName() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setName(""))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Name must be not null and not empty")));
    }

    @Test
    void saveWithNoCorrectProductionDate() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto()
                                .setProductionDate(Instant.now().plusSeconds(1)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]",
                        is("Production date cannot be later than current date and time")));
    }

    @Test
    void saveWithNoCorrectProducerId() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setProducerId(-1))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Producer with this ID does not exist")));
    }

    @Test
    void saveWithNoCorrectPriceInCents() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setPriceInCents(0))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Price (in cents) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectNumberOfSheets() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/punches")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setMaxSheets(9))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Number of Sheets starting from 10 and more")));
    }

    @Test
    void update() throws Exception {
        when(service.update(anyInt(), any(PuncherDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.put("/punches" + "/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.puncherKnives", is(PUNCHER_KNIVES.toString())))
                .andExpect(jsonPath("$.maxSheets", is(MAX_SHEETS)))
                .andExpect(jsonPath("$.discount", is(DISCOUNT)));
    }

    @Test
    void updateWithDuplicationFound() throws Exception {
        doThrow(EntityDuplicationException.class)
                .when(service).update(anyInt(), any(PuncherDto.class));

        mvc.perform(MockMvcRequestBuilders.put("/punches" + "/{id}", 1)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")));
    }

    @Test
    void deleteByID() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/punches" + "/{id}", 1))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private PuncherDto generateDto() {
        return new PuncherDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setPuncherKnives(PUNCHER_KNIVES)
                .setMaxSheets(MAX_SHEETS)
                .setDiscount(DISCOUNT);
    }
}