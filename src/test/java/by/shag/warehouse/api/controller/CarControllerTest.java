package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.CarsCarcase;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@WebMvcTest(controllers = CarController.class)
class CarControllerTest {

    private static final Country COUNTRY = new Country().setId(2).setName("Беларусь");
    private static final Producer PRODUCER = new Producer().setId(3).setName("Живет");

    private static final Integer ID = 1;
    private static final String NAME = "Bugatti";
    private static final Integer PRICE_IN_CENTS = 560000;
    private static final Instant PRODUCTION_DATE = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 7;
    private static final Boolean ELECTRICAL = true;
    private static final CarsCarcase CARCASE = CarsCarcase.SEDAN;

    @MockBean
    private CarController controller;

    @Autowired
    private MockMvc mvc;

    @Autowired
    private ObjectMapper mapper;

    @MockBean
    private CountryRepository countryRepository;

    @MockBean
    private ProducerRepository producerRepository;

    @Test
    void findById() throws Exception {
        when(controller.findById(anyInt())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/car" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.electrical", is(ELECTRICAL)))
                .andExpect(jsonPath("$.carcase", is(CARCASE.toString())));
    }

    @Test
    void findAll() throws Exception {
        when(controller.findAll()).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/car"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].name", is(NAME)))
                .andExpect(jsonPath("$.[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[0].productionDate", is(PRODUCTION_DATE.toString())))

                .andExpect(jsonPath("$.[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[0].electrical", is(ELECTRICAL)))
                .andExpect(jsonPath("$.[0].carcase", is(CARCASE.toString())))

                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].name", is(NAME)))
                .andExpect(jsonPath("$.[1].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[1].productionDate", is(PRODUCTION_DATE.toString())))

                .andExpect(jsonPath("$.[1].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[1].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[1].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[1].electrical", is(ELECTRICAL)))
                .andExpect(jsonPath("$.[1].carcase", is(CARCASE.toString())));
    }

    @Test
    void save() throws Exception {
        when(controller.save(any(CarDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.electrical", is(ELECTRICAL)))
                .andExpect(jsonPath("$.carcase", is(CARCASE.toString())));
    }

    @Test
    void saveWithNullData() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new CarDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message",hasSize(8)))
                .andExpect(jsonPath("$.message", containsInAnyOrder(
                        "Electrical must be not null",
                        "Production date must be not null",
                        "Carcase must be not null",
                        "Name must be not null and not empty",
                        "Price (in cents) must be not null",
                        "Guaranty (in months) must be not null",
                        "Producer with this ID does not exist",
                        "Country with this ID does not exist")));
    }

    @Test
    void saveWithEmptyName() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setName(""))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Name must be not null and not empty")));
    }

    @Test
    void saveWithNoCorrectPriceInCents() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setPriceInCents((int) (Math.random() * -100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Price (in cents) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectProductionDate() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setProductionDate(Instant.now().plusSeconds((int) ((Math.random() * 10e6) + 10e2))))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Production date cannot be later than current date and time")));
    }

    @Test
    void saveWithNoCorrectGuarantyInMonths() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setGuarantyInMonths((int) (Math.random() * -100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Guaranty (in months) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectCountryId() throws Exception {
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setCountryId((int) (Math.random() * 100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Country with this ID does not exist")));
    }

    @Test
    void saveWithNoCorrectProducerId() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));

        mvc.perform(MockMvcRequestBuilders.post("/car")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setProducerId((int) (Math.random() * 100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Producer with this ID does not exist")));
    }


    @Test
    void update() throws Exception {
        when(controller.update(anyInt(), any(CarDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.put("/car" + "/{id}", (int) (Math.random() * 100))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DATE.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.electrical", is(ELECTRICAL)))
                .andExpect(jsonPath("$.carcase", is(CARCASE.toString())));
    }

    @Test
    void updateWithDuplicationFound() throws Exception {
        doThrow(EntityDuplicationException.class).when(controller).update(anyInt(), any(CarDto.class));

        mvc.perform(MockMvcRequestBuilders.put("/car" + "/{id}", (int) (Math.random() * 100))
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")));
    }

    @Test
    void delete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/car" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private CarDto generateDto() {
        return new CarDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setElectrical(ELECTRICAL)
                .setCarcase(CARCASE);
    }
}