package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.PowerSmell;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import by.shag.warehouse.service.GenaBukinSocksService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.Arrays;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = GenaBukinSocksController.class)
class GenaBukinSocksControllerTest {

    private static final Integer ID = 1;
    private static final String NAME = "Socks";
    private static final Integer PRICE_IN_CENTS = 50;
    private static final Instant PRODUCTION_DAY = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 1;
    private static final Country COUNTRY = new Country().setId(2).setName("BLR");
    private static final Producer PRODUCER = new Producer().setId(3).setName("RUS");
    private static final Integer HOLES_QUANTITY = 10;
    private static final PowerSmell POWER_SMELL = PowerSmell.DEADLY;
    private static final String FABRIC = "HB";


    @MockBean
    private GenaBukinSocksService service;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper mapper;
    @MockBean
    private CountryRepository countryRepository;
    @MockBean
    private ProducerRepository producerRepository;

    @Test
    void findById() throws Exception {
        when(service.findById(anyInt())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/socks" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.holesQuantity", is(HOLES_QUANTITY)))
                .andExpect(jsonPath("$.powerSmell", is(POWER_SMELL.toString())))
                .andExpect(jsonPath("$.fabric", is(FABRIC)));
    }

    @Test
    void findAll() throws Exception {
        when(service.findAll(any())).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/socks"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].name", is(NAME)))
                .andExpect(jsonPath("$.[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[0].holesQuantity", is(HOLES_QUANTITY)))
                .andExpect(jsonPath("$.[0].powerSmell", is(POWER_SMELL.toString())))
                .andExpect(jsonPath("$.[0].fabric", is(FABRIC)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].name", is(NAME)))
                .andExpect(jsonPath("$.[1].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[1].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.[1].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[1].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[1].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[1].holesQuantity", is(HOLES_QUANTITY)))
                .andExpect(jsonPath("$.[1].powerSmell", is(POWER_SMELL.toString())))
                .andExpect(jsonPath("$.[1]fabric", is(FABRIC)));
    }

    @Test
    void save() throws Exception {
        when(service.save(any(GenaBukinSocksDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.holesQuantity", is(HOLES_QUANTITY)))
                .andExpect(jsonPath("$.powerSmell", is(POWER_SMELL.toString())))
                .andExpect(jsonPath("$.fabric", is(FABRIC)));
    }

    @Test
    void saveWithNullFields() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(new GenaBukinSocksDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", hasSize(9)))
                .andExpect(jsonPath("$.message", containsInAnyOrder(
                        "Guaranty should be not null",
                        "Holes quantity should be not null",
                        "Producer with this ID does not exist",
                        "Price should be not null",
                        "Production date should be not null",
                        "Name should be not null or empty",
                        "Power smell should be not null",
                        "Country with this ID does not exist",
                        "Fabric should be not null")));
    }

    @Test
    void saveWithoutName() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setName(" "))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Name should be not null or empty")));
    }

    @Test
    void saveWhenPriceIncorrect() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setPriceInCents(-5))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Price should be more than 0")));
    }

    @Test
    void saveWhenProductionDateIncorrect() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setProductionDate(Instant.now().plusSeconds(5)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Production date cannot be later than current date and time")));
    }

    @Test
    void saveWhenGuarantyIncorrect() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setGuarantyInMonths(-5))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Guaranty should be more than 0")));
    }

    @Test
    void saveWhenCountryIdIncorrect() throws Exception {
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setCountryId(5))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Country with this ID does not exist")));
    }

    @Test
    void saveWhenProducerIdIncorrect() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));

        mvc.perform(MockMvcRequestBuilders.post("/socks")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setProducerId(5))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Producer with this ID does not exist")));
    }

    @Test
    void update() throws Exception {
        when(service.update(any(GenaBukinSocksDto.class), anyInt())).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.put("/socks" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.holesQuantity", is(HOLES_QUANTITY)))
                .andExpect(jsonPath("$.powerSmell", is(POWER_SMELL.toString())))
                .andExpect(jsonPath("$.fabric", is(FABRIC)));
    }

    @Test
    void updateWithDuplicationFound() throws Exception {
        doThrow(EntityDuplicationException.class).when(service).update(any(GenaBukinSocksDto.class), anyInt());

        mvc.perform(MockMvcRequestBuilders.put("/socks" + "/{id}", 5)
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(mapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")));
    }

    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/socks" + "/{id}", 5))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private GenaBukinSocksDto generateDto() {
        return new GenaBukinSocksDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setHolesQuantity(HOLES_QUANTITY)
                .setPowerSmell(POWER_SMELL)
                .setFabric(FABRIC);
    }
}