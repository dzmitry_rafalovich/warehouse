package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = ProducerController.class)
class ProducerControllerTest {

    @MockBean
    private ProducerController controller;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void findById() throws Exception {
        when(controller.findById(anyInt())).thenReturn(new ProducerDto().setId(1).setName("Test Name"));

        mvc.perform(MockMvcRequestBuilders.get("/producers" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Test Name")));
    }

    @Test
    void findAll() throws Exception {
        when(controller.findAll()).thenReturn(Arrays.asList(
                new ProducerDto().setId(1).setName("Test Name"),
                new ProducerDto().setId(2).setName("Test Name 2")
        ));

        mvc.perform(MockMvcRequestBuilders.get("/producers"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(1)))
                .andExpect(jsonPath("$.[0].name", is("Test Name")))
                .andExpect(jsonPath("$.[1].id", is(2)))
                .andExpect(jsonPath("$.[1].name", is("Test Name 2")));
    }

    @Test
    void save() throws Exception {
        when(controller.save(any(ProducerDto.class))).thenReturn(new ProducerDto().setId(1).setName("Test Name"));

        mvc.perform(MockMvcRequestBuilders.post("/producers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new ProducerDto().setId(2).setName("Test Name 2"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Test Name")));
    }

    @Test
    void saveWithoutName() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/producers")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new ProducerDto().setId(2))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Name must be not null and not empty")));
    }

    @Test
    void update() throws Exception {
        when(controller.update(anyInt(), any(ProducerDto.class))).thenReturn(new ProducerDto().setId(1).setName("Test Name"));

        mvc.perform(MockMvcRequestBuilders.put("/producers" + "/{id}", (int) (Math.random() * 100))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new CountryDto().setName("Test Name 2"))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.name", is("Test Name")));
    }

    @Test
    void updateWithDuplicationFound() throws Exception {
        doThrow(EntityDuplicationException.class).when(controller).update(anyInt(), any(ProducerDto.class));

        mvc.perform(MockMvcRequestBuilders.put("/producers" + "/{id}", (int) (Math.random() * 100))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new ProducerDto().setId(1).setName("Test Name"))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")));
    }

    @Test
    void delete() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/producers" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk());
    }
}