package by.shag.warehouse.api.controller;

import by.shag.warehouse.api.dto.BasketPencilDto;
import by.shag.warehouse.api.dto.ItemPencilDto;
import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.jpa.repository.PencilRepository;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import by.shag.warehouse.service.PencilService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.time.Instant;
import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(controllers = PencilController.class)
class PencilControllerTest {

    private static final Integer ID = 1;
    private static final String NAME = "Test_Name";
    private static final Integer PRICE_IN_CENTS = 100;
    private static final Instant PRODUCTION_DAY = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 10;
    private static final Country COUNTRY = new Country().setId(2).setName("Test Country Name");
    private static final Producer PRODUCER = new Producer().setId(3).setName("Test Producer Name");
    private static final Boolean ERASER = true;
    private static final PencilHardness HARDNESS = PencilHardness.B;
    private static final String COLOR = "Test_color";
    private static final Integer EXPECTATION = 100001;

    @MockBean
    private PencilService service;
    @Autowired
    private MockMvc mvc;
    @Autowired
    private ObjectMapper objectMapper;
    @MockBean
    private CountryRepository countryRepository;
    @MockBean
    private ProducerRepository producerRepository;
    @MockBean
    private PencilRepository repository;

    @Test
    void findById() throws Exception {
        when(service.findById(anyInt())).thenReturn(generateDto());

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.eraser", is(ERASER)))
                .andExpect(jsonPath("$.hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$.color", is(COLOR)));
    }

    @Test
    void findAll() throws Exception {
        when(service.findAll(any(PencilSearchCriteriaDto.class))).thenReturn(Arrays.asList(generateDto(), generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(ID)))
                .andExpect(jsonPath("$.[0].name", is(NAME)))
                .andExpect(jsonPath("$.[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$.[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$.[0].color", is(COLOR)))
                .andExpect(jsonPath("$.[1].id", is(ID)))
                .andExpect(jsonPath("$.[1].name", is(NAME)))
                .andExpect(jsonPath("$.[1].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.[1].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.[1].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.[1].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.[1].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.[1].eraser", is(ERASER)))
                .andExpect(jsonPath("$.[1].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$.[1].color", is(COLOR)));
    }

    @Test
    void save() throws Exception {
        when(service.save(any(PencilDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.eraser", is(ERASER)))
                .andExpect(jsonPath("$.hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$.color", is(COLOR)));
    }

    @Test
    void saveWithNullData() throws Exception {
        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(new PencilDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", hasSize(9)))
                .andExpect(jsonPath("$.message", containsInAnyOrder("Eraser must be not null",
                        "Production date must be not null",
                        "Hardness must be not null",
                        "Name must be not null and not empty",
                        "Price (in cents) must be not null",
                        "Guaranty (in months) must be not null",
                        "Producer with this ID does not exist",
                        "Country with this ID does not exist",
                        "Color must be not null and not empty")));
    }

    @Test
    void saveWithEmptyName() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setName(""))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Name must be not null and not empty")));
    }

    @Test
    void saveWithNoCorrectPriceInCents() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setPriceInCents((int) (Math.random() * -100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Price (in cents) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectProductionDate() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setProductionDate(Instant.now().plusSeconds((int) ((Math.random() * 10e6) + 10e2))))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Production date cannot be later than current date and time")));
    }

    @Test
    void saveWithNoCorrectGuarantyInMonths() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setGuarantyInMonths((int) (Math.random() * -100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Guaranty (in months) must be more than 0")));
    }

    @Test
    void saveWithNoCorrectCountryId() throws Exception {
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setCountryId((int) (Math.random() * 100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Country with this ID does not exist")));
    }

    @Test
    void saveWithNoCorrectProducerId() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setProducerId((int) (Math.random() * 100)))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Producer with this ID does not exist")));
    }

    @Test
    void saveWithNoCorrectHardness() throws Exception {
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.post("/pencils")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{\"countryId\": 1, \"eraser\": true, \"guarantyInMonths\": 12, \"hardness\": \"A\"," +
                        " \"name\": \"Test\", \"priceInCents\": 10, \"producerId\": 1, \"productionDate\": \"2021-08-31T00:00:00.0Z\", \"color\": \"Color\"}"))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message[0]", is("Cannot deserialize value of type `by.shag.warehouse.jpa.model.PencilHardness` " +
                        "from String \"A\": not one of the values accepted for Enum class: [H, B, F, HB]\n at [Source: (PushbackInputStream); " +
                        "line: 1, column: 70] (through reference chain: by.shag.warehouse.api.dto.PencilDto[\"hardness\"])")));
    }

    @Test
    void update() throws Exception {
        when(service.update(anyInt(), any(PencilDto.class))).thenReturn(generateDto());
        when(countryRepository.findById(anyInt())).thenReturn(Optional.of(COUNTRY));
        when(producerRepository.findById(anyInt())).thenReturn(Optional.of(PRODUCER));

        mvc.perform(MockMvcRequestBuilders.put("/pencils" + "/{id}", (int) (Math.random() * 100))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto().setId(null))))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(ID)))
                .andExpect(jsonPath("$.name", is(NAME)))
                .andExpect(jsonPath("$.priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$.productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$.guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$.countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$.producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$.eraser", is(ERASER)))
                .andExpect(jsonPath("$.hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$.color", is(COLOR)));
    }

    @Test
    void updateWithDuplicationFound() throws Exception {
        doThrow(EntityDuplicationException.class).when(service).update(anyInt(), any(PencilDto.class));

        mvc.perform(MockMvcRequestBuilders.put("/pencils" + "/{id}", (int) (Math.random() * 100))
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(generateDto())))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("BAD_REQUEST")));
    }

    @Test
    void deleteById() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/pencils" + "/{id}", (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private PencilDto generateDto() {
        return new PencilDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setEraser(ERASER)
                .setHardness(HARDNESS)
                .setColor(COLOR);
    }

    @Test
    void calculateTheBasket() throws Exception {
        BasketPencilDto data = new BasketPencilDto()
                .setPencilDtoList(Arrays.asList(new ItemPencilDto().setId(1).setQuantity(1),
                        new ItemPencilDto().setId(2).setQuantity(2)));

        when(repository.findById(anyInt())).thenReturn(Optional.of(new Pencil()));
        when(service.calculateTheBasket(any(BasketPencilDto.class))).thenReturn(EXPECTATION);

        MvcResult result = mvc.perform(MockMvcRequestBuilders.post("/pencils" + "/basket")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(data)))
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn();

        assertEquals(result.getResponse().getContentAsString(), EXPECTATION.toString());
    }

    @Test
    void calculateTheBasketWithIncorrectData() throws Exception {
        BasketPencilDto data = new BasketPencilDto()
                .setPencilDtoList(Arrays.asList(new ItemPencilDto(),
                        new ItemPencilDto().setId(-1).setQuantity(-1)));

        mvc.perform(MockMvcRequestBuilders.post("/pencils" + "/basket")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(data)))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", hasSize(4)))
                .andExpect(jsonPath("$.message", containsInAnyOrder(
                        "Pencil with this ID does not exist",
                        "Quantity must be not null",
                        "Pencil with this ID does not exist",
                        "Quantity must be greater than or equal to 0")));
    }

    @Test
    void findByPriceInCentsBetween() throws Exception {
        when(service.findByPriceInCentsBetween(anyInt(), anyInt())).thenReturn(Collections.singletonList(generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/search/priceInCents/{minPrice}/{maxPrice}", (int) (Math.random() * 100), (int) (Math.random() * 100)))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ID)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$[0].color", is(COLOR)));
    }

    @Test
    void findByHardnessAndEraser() throws Exception {
        when(service.findByHardnessAndEraser(any(PencilHardness.class), any(Boolean.class))).thenReturn(Collections.singletonList(generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/search/hardnessAndEraser/{hardness}/{eraser}", PencilHardness.B, true))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ID)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$[0].color", is(COLOR)));
    }

    @Test
    void findByNameStartingWithIgnoreCase() throws Exception {
        when(service.findByNameStartingWithIgnoreCase(any(String.class))).thenReturn(Collections.singletonList(generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/search/nameStartingWithIgnoreCase/{name}", "QW"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ID)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$[0].color", is(COLOR)));
    }

    @Test
    void findByProductionDateBefore() throws Exception {
        when(service.findByProductionDateBefore(any(Instant.class))).thenReturn(Collections.singletonList(generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/search/productionDateBefore/{productionDate}", "2001-01-01T00:00:00Z"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ID)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$[0].color", is(COLOR)));
    }

    @Test
    void findByCountryNameContainingOrProducerNameContaining() throws Exception {
        when(service.findByCountryNameContainingOrProducerNameContaining(any(String.class), any(String.class))).thenReturn(Collections.singletonList(generateDto()));

        mvc.perform(MockMvcRequestBuilders.get("/pencils" + "/search/countryNameContainingOrProducerNameContaining/{countryName}/{producerName}}", "A", "B"))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].id", is(ID)))
                .andExpect(jsonPath("$[0].name", is(NAME)))
                .andExpect(jsonPath("$[0].priceInCents", is(PRICE_IN_CENTS)))
                .andExpect(jsonPath("$[0].productionDate", is(PRODUCTION_DAY.toString())))
                .andExpect(jsonPath("$[0].guarantyInMonths", is(GUARANTY_IN_MONTHS)))
                .andExpect(jsonPath("$[0].countryId", is(COUNTRY.getId())))
                .andExpect(jsonPath("$[0].producerId", is(PRODUCER.getId())))
                .andExpect(jsonPath("$[0].eraser", is(ERASER)))
                .andExpect(jsonPath("$[0].hardness", is(HARDNESS.toString())))
                .andExpect(jsonPath("$[0].color", is(COLOR)));
    }
}