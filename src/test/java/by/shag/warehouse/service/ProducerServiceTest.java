package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.repository.ProducerRepository;
import by.shag.warehouse.mapping.ProducerMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class ProducerServiceTest {

    private static final Integer ID = 1;
    @Mock
    private ProducerRepository repository;
    @Mock
    private ProducerMapper mapper;
    @InjectMocks
    private ProducerService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        Producer mapped = mock(Producer.class);
        when(mapper.mapFromDto(any(ProducerDto.class))).thenReturn(mapped);

        Producer saved = mock(Producer.class);
        when(repository.save(any(Producer.class))).thenReturn(saved);

        ProducerDto producerDtoReturn = mock(ProducerDto.class);
        when(mapper.mapFromModel(any(Producer.class))).thenReturn(producerDtoReturn);

        ProducerDto producerDto = mock(ProducerDto.class);
        ProducerDto result = service.save(producerDto);

        assertEquals(result, producerDtoReturn);

        verify(repository).findByName(producerDto.getName());
        verify(mapper).mapFromDto(producerDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void saveAndUpdateWithException() {
        String countryName = "Test country";
        when(repository.findByName(anyString())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> repository.findByName(countryName));

        verify(repository).findByName(countryName);
    }


    @Test
    void findById() {
        Producer producer = mock(Producer.class);
        Optional<Producer> optionalProducer = Optional.of(producer);
        when(repository.findById(ID)).thenReturn(optionalProducer);

        ProducerDto mapped = mock(ProducerDto.class);
        when(mapper.mapFromModel(any(Producer.class))).thenReturn(mapped);

        ProducerDto result = service.findById(ID);
        assertEquals(result, mapped);

        verify(repository).findById(ID);
        verify(mapper).mapFromModel(producer);
    }

    @Test
    void findByIdWhenNoSuchProducer() {
        assertThrows(EntityNotFoundException.class,
                () -> service.findById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void findAll() {
        Producer model = mock(Producer.class);
        List<Producer> modelList = Arrays.asList(model, model, model);
        when(repository.findAll()).thenReturn(modelList);

        ProducerDto mapped = mock(ProducerDto.class);
        List<ProducerDto> mappedList = Arrays.asList(mapped, mapped, mapped);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        List<ProducerDto> result = service.findAll();
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAll();
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        Producer mapped = mock(Producer.class);
        when(mapper.mapFromDto(any(ProducerDto.class))).thenReturn(mapped);

        Producer updated = mock(Producer.class);
        when(repository.save(any(Producer.class))).thenReturn(updated);

        ProducerDto dtoReturned = mock(ProducerDto.class);
        when(mapper.mapFromModel(any(Producer.class))).thenReturn(dtoReturned);

        ProducerDto producerDto = mock(ProducerDto.class);
        ProducerDto result = service.update(ID, producerDto);

        assertEquals(result, dtoReturned);

        verify(repository).findByName(producerDto.getName());
        verify(mapper).mapFromDto(producerDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        service.deleteById(ID);

        verify(repository).deleteById(ID);
    }
}