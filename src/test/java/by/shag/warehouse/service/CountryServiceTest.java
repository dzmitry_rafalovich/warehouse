package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.repository.CountryRepository;
import by.shag.warehouse.mapping.CountryMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class CountryServiceTest {

    private static final Integer ID = 1;
    @Mock
    private CountryRepository repository;
    @Mock
    private CountryMapper mapper;
    @InjectMocks
    private CountryService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        Country mapped = mock(Country.class);
        when(mapper.mapFromDto(any(CountryDto.class))).thenReturn(mapped);

        Country saved = mock(Country.class);
        when(repository.save(any(Country.class))).thenReturn(saved);

        CountryDto countryDtoReturn = mock(CountryDto.class);
        when(mapper.mapFromModel(any(Country.class))).thenReturn(countryDtoReturn);

        CountryDto countryDto = mock(CountryDto.class);
        CountryDto result = service.save(countryDto);

        assertEquals(result, countryDtoReturn);

        verify(repository).findByName(countryDto.getName());
        verify(mapper).mapFromDto(countryDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void saveAndUpdateWithException() {
        String countryName = "Test country";
        when(repository.findByName(anyString())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> repository.findByName(countryName));

        verify(repository).findByName(countryName);
    }

    @Test
    void findById() {
        Country country = mock(Country.class);
        Optional<Country> optionalCountry = Optional.of(country);
        when(repository.findById(ID)).thenReturn(optionalCountry);

        CountryDto mapped = mock(CountryDto.class);
        when(mapper.mapFromModel(any(Country.class))).thenReturn(mapped);

        CountryDto result = service.findById(ID);
        assertEquals(result, mapped);

        verify(repository).findById(ID);
        verify(mapper).mapFromModel(country);

    }

    @Test
    void findByIdWhenNoSuchCountry() {
        assertThrows(EntityNotFoundException.class,
                () -> service.findById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void findAll() {
        Country model = mock(Country.class);
        List<Country> modelList = Arrays.asList(model, model, model);
        when(repository.findAll()).thenReturn(modelList);

        CountryDto mapped = mock(CountryDto.class);
        List<CountryDto> mappedList = Arrays.asList(mapped, mapped, mapped);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        List<CountryDto> result = service.findAll();
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAll();
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        Country mapped = mock(Country.class);
        when(mapper.mapFromDto(any(CountryDto.class))).thenReturn(mapped);

        Country updated = mock(Country.class);
        when(repository.save(any(Country.class))).thenReturn(updated);

        CountryDto dtoReturned = mock(CountryDto.class);
        when(mapper.mapFromModel(any(Country.class))).thenReturn(dtoReturned);

        CountryDto countryDto = mock(CountryDto.class);
        CountryDto result = service.update(ID, countryDto);

        assertEquals(result, dtoReturned);

        verify(repository).findByName(countryDto.getName());
        verify(mapper).mapFromDto(countryDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        service.deleteById(ID);

        verify(repository).deleteById(ID);
    }
}