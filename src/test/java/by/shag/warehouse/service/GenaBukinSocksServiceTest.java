package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.GenaBukinSocksCriteriaDto;
import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import by.shag.warehouse.jpa.repository.GenaBukinSocksRepository;
import by.shag.warehouse.mapping.GenaBukinSocksMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class GenaBukinSocksServiceTest {

    private static final Integer ID = 1;

    @Mock
    private GenaBukinSocksRepository repository;
    @Mock
    private GenaBukinSocksMapper mapper;
    @InjectMocks
    private GenaBukinSocksService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        GenaBukinSocks mapped = mock(GenaBukinSocks.class);
        when(mapper.mapFromDto(any(GenaBukinSocksDto.class))).thenReturn(mapped);

        GenaBukinSocks saved = mock(GenaBukinSocks.class);
        when(repository.save(any(GenaBukinSocks.class))).thenReturn(saved);

        GenaBukinSocksDto genaBukinSocksDtoReturn = mock(GenaBukinSocksDto.class);
        when(mapper.mapFromModel(any(GenaBukinSocks.class))).thenReturn(genaBukinSocksDtoReturn);

        GenaBukinSocksDto genaBukinSocksDto = mock(GenaBukinSocksDto.class);
        GenaBukinSocksDto result = service.save(genaBukinSocksDto);

        assertEquals(result, genaBukinSocksDtoReturn);

        verify(repository).findByName(genaBukinSocksDto.getName(), genaBukinSocksDto.getProducerId());
        verify(mapper).mapFromDto(genaBukinSocksDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void findById() {
        GenaBukinSocks genaBukinSocks = mock(GenaBukinSocks.class);
        Optional<GenaBukinSocks> optionalGenaBukinSocks = Optional.of(genaBukinSocks);
        when(repository.findById(ID)).thenReturn(optionalGenaBukinSocks);

        GenaBukinSocksDto mapped = mock(GenaBukinSocksDto.class);
        when(mapper.mapFromModel(any(GenaBukinSocks.class))).thenReturn(mapped);

        GenaBukinSocksDto result = service.findById(ID);
        assertEquals(result, mapped);

        verify(repository).findById(ID);
        verify(mapper).mapFromModel(genaBukinSocks);
    }

    @Test
    void findByIdWhenNoSuchGenaBukinSocks() {
        assertThrows(EntityNotFoundException.class,
                () -> service.findById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void findAll() {
        GenaBukinSocks model = mock(GenaBukinSocks.class);
        List<GenaBukinSocks> modelList = Arrays.asList(model, model, model);
        when(repository.findAll(any(GenaBukinSocksCriteriaDto.class))).thenReturn(modelList);

        GenaBukinSocksDto mapped = mock(GenaBukinSocksDto.class);
        List<GenaBukinSocksDto> mappedList = Arrays.asList(mapped, mapped, mapped);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        GenaBukinSocksCriteriaDto criteriaDto = mock(GenaBukinSocksCriteriaDto.class);
        List<GenaBukinSocksDto> result = service.findAll(criteriaDto);
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAll(criteriaDto);
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        GenaBukinSocks mapped = mock(GenaBukinSocks.class);
        when(mapper.mapFromDto(any(GenaBukinSocksDto.class))).thenReturn(mapped);

        GenaBukinSocks updated = mock(GenaBukinSocks.class);
        when(repository.update(any(GenaBukinSocks.class))).thenReturn(updated);

        GenaBukinSocksDto dtoReturned = mock(GenaBukinSocksDto.class);
        when(mapper.mapFromModel(any(GenaBukinSocks.class))).thenReturn(dtoReturned);

        GenaBukinSocksDto genaBukinSocksDto = mock(GenaBukinSocksDto.class);
        GenaBukinSocksDto result = service.update(genaBukinSocksDto, ID);

        assertEquals(result, dtoReturned);

        verify(repository).findByName(genaBukinSocksDto.getName(), genaBukinSocksDto.getId());
        verify(mapper).mapFromDto(genaBukinSocksDto);
        verify(repository).update(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        GenaBukinSocks genaBukinSocks = mock(GenaBukinSocks.class);
        Optional<GenaBukinSocks> optionalGenaBukinSocks = Optional.of(genaBukinSocks);

        when(repository.findById(anyInt())).thenReturn(optionalGenaBukinSocks);

        service.deleteById(ID);

        verify(repository).findById(ID);
        verify(repository).deleteById(genaBukinSocks.getId());
    }

    @Test
    void deleteByIdWithException() {
        assertThrows(EntityNotFoundException.class, () -> service.deleteById(ID));

        verify(repository).findById(ID);
    }
}