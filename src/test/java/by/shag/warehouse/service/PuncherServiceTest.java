package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.api.dto.PuncherSearchCriteriaDto;
import by.shag.warehouse.exception.EntityDuplicationException;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Puncher;
import by.shag.warehouse.jpa.repository.PuncherRepository;
import by.shag.warehouse.mapping.PuncherMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PuncherServiceTest {

    @Mock
    private PuncherRepository repository;
    @Mock
    private PuncherMapper mapper;
    @InjectMocks
    private PuncherService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        Puncher mapped = mock(Puncher.class);
        when(mapper.mapFromDto(any(PuncherDto.class))).thenReturn(mapped);

        Puncher saved = mock(Puncher.class);
        when(repository.save(any(Puncher.class))).thenReturn(saved);

        PuncherDto puncherDtoReturn = mock(PuncherDto.class);
        when(mapper.mapFromModel(any(Puncher.class))).thenReturn(puncherDtoReturn);

        PuncherDto puncherDto = mock(PuncherDto.class);
        PuncherDto result = service.save(puncherDto);

        assertEquals(result, puncherDtoReturn);

        verify(repository).findByNameAndProducer(puncherDto.getName(), puncherDto.getProducerId());
        verify(mapper).mapFromDto(puncherDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void findById() {
        Integer id = 1;

        Puncher puncher = mock(Puncher.class);
        Optional<Puncher> optionalPuncher = Optional.of(puncher);
        when(repository.findById(id)).thenReturn(optionalPuncher);

        PuncherDto mapped = mock(PuncherDto.class);
        when(mapper.mapFromModel(any(Puncher.class))).thenReturn(mapped);

        PuncherDto result = service.findById(id);
        assertEquals(result, mapped);

        verify(repository).findById(id);
        verify(mapper).mapFromModel(puncher);
    }

    @Test
    void findByIdWhenNoSuchPuncher() {
        Integer id = 1;

        assertThrows(EntityNotFoundException.class,
                () -> service.findById(id));

        verify(repository).findById(id);
    }

    @Test
    void saveAndUpdateWhenNoSuchPuncherAndProducer() {
        Integer idProducer = 1;
        String dtoName = "Puncher name";

        when(repository.findByNameAndProducer(anyString(),
                anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> repository.findByNameAndProducer(dtoName, idProducer));

        verify(repository).findByNameAndProducer(dtoName, idProducer);
    }

    @Test
    void saveAndUpdateWhenDuplicateWasSent() {
        Integer idProducer = 1;
        String dtoName = "Puncher name";

        when(repository.findByNameAndProducer(dtoName,
                idProducer)).thenThrow(EntityDuplicationException.class);

        assertThrows(EntityDuplicationException.class,
                () -> repository.findByNameAndProducer(dtoName, idProducer));

        verify(repository).findByNameAndProducer(dtoName, idProducer);
    }

    @Test
    void findAll() {

        Puncher model = mock(Puncher.class);
        List<Puncher> modelList = Arrays.asList(model, model);
        when(repository.findAll(any(PuncherSearchCriteriaDto.class)))
                .thenReturn(modelList);

        PuncherDto mapped = mock(PuncherDto.class);
        List<PuncherDto> mappedList = Arrays.asList(mapped, mapped);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        PuncherSearchCriteriaDto criteriaDto = mock(PuncherSearchCriteriaDto.class);
        List<PuncherDto> result = service.findAll(criteriaDto);
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAll(criteriaDto);
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        Integer id = 1;

        Puncher mapped = mock(Puncher.class);
        when(mapper.mapFromDto(any(PuncherDto.class))).thenReturn(mapped);

        Puncher updated = mock(Puncher.class);
        when(repository.update(any(Puncher.class))).thenReturn(updated);

        PuncherDto dtoReturned = mock(PuncherDto.class);
        when(mapper.mapFromModel(any(Puncher.class))).thenReturn(dtoReturned);

        PuncherDto puncherDto = mock(PuncherDto.class);
        PuncherDto result = service.update(id, puncherDto);

        assertEquals(result, dtoReturned);

        verify(repository).findByNameAndProducer(puncherDto.getName(), puncherDto.getId());
        verify(mapper).mapFromDto(puncherDto);
        verify(repository).update(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        Integer id = 1;

        Puncher puncher = mock(Puncher.class);
        Optional<Puncher> optionalPuncher = Optional.of(puncher);
        when(repository.findById(anyInt())).thenReturn(optionalPuncher);

        PuncherDto mapped = mock(PuncherDto.class);
        when(mapper.mapFromModel(any(Puncher.class))).thenReturn(mapped);

        service.deleteById(id);

        verify(repository).deleteById(mapped.getId());
        verify(repository).findById(id);
        verify(mapper).mapFromModel(puncher);
    }
}