package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Car;
import by.shag.warehouse.jpa.repository.CarRepository;
import by.shag.warehouse.mapping.CarMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class CarServiceTest {

    private static final Integer ID = 1;

    @Mock
    private CarRepository repository;
    @Mock
    private CarMapper mapper;
    @InjectMocks
    private CarService service;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }


    @Test
    void save() {
        Car mapped = mock(Car.class);
        when(mapper.mapFromDto(any(CarDto.class))).thenReturn(mapped);

        Car saved = mock(Car.class);
        when(repository.save(any(Car.class))).thenReturn(saved);

        CarDto CarDtoReturn = mock(CarDto.class);
        when(mapper.mapFromModel(any(Car.class))).thenReturn(CarDtoReturn);

        CarDto carDto = mock(CarDto.class);
        CarDto result = service.save(carDto);

        assertEquals(result, CarDtoReturn);

        verify(repository).findByName(carDto.getName(), carDto.getProducerId());
        verify(mapper).mapFromDto(carDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void findById() {
        Car car = mock(Car.class);
        Optional<Car> optionalCar = Optional.of(car);
        when(repository.findById(ID)).thenReturn(optionalCar);

        CarDto mapped = mock(CarDto.class);
        when(mapper.mapFromModel(any(Car.class))).thenReturn(mapped);

        CarDto result = service.findById(ID);
        assertEquals(result, mapped);

        verify(repository).findById(ID);
        verify(mapper).mapFromModel(car);
    }

    @Test
    void findByIdWhenNoCar() {
        assertThrows(EntityServiceException.class,
                () -> service.findById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void findAll() {
        Car model = mock(Car.class);
        List<Car> modelList = Arrays.asList(model, model, model);
        when(repository.findAll()).thenReturn(modelList);

        CarDto mapped = mock(CarDto.class);
        List<CarDto> mappedList = Arrays.asList(mapped, mapped, mapped);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        List<CarDto> result = service.findAll();
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAll();
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        Car mapped = mock(Car.class);
        when(mapper.mapFromDto(any(CarDto.class))).thenReturn(mapped);

        Car updated = mock(Car.class);
        when(repository.update(any(Car.class))).thenReturn(updated);

        CarDto dtoReturned = mock(CarDto.class);
        when(mapper.mapFromModel(any(Car.class))).thenReturn(dtoReturned);

        CarDto carDto = mock(CarDto.class);
        CarDto result = service.update(ID, carDto);

        assertEquals(result, dtoReturned);

        verify(repository).findByName(carDto.getName(), carDto.getProducerId());
        verify(mapper).mapFromDto(carDto);
        verify(repository).update(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        service.deleteById(ID);
        verify(repository).deleteById(ID);
    }

    @Test
    void saveAndUpdateWithException() {
        String carName = "Test pencil name";
        when(repository.findByName(anyString(), anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> repository.findByName(carName, ID));

        verify(repository).findByName(carName, ID);
    }
}