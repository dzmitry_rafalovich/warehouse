package by.shag.warehouse.service;

import by.shag.warehouse.api.dto.BasketPencilDto;
import by.shag.warehouse.api.dto.ItemPencilDto;
import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.exception.EntityNotFoundException;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import by.shag.warehouse.jpa.repository.PencilRepository;
import by.shag.warehouse.mapping.PencilMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PencilServiceTest {

    private static final Integer ID = 1;
    @Mock
    private PencilRepository repository;
    @Mock
    private PencilMapper mapper;
    @InjectMocks
    private PencilService service;

    private List<Pencil> modelList;
    private List<PencilDto> mappedList;

    @BeforeEach
    void before() {
        Pencil model = mock(Pencil.class);
        PencilDto modelDto = mock(PencilDto.class);
        modelList = Arrays.asList(model, model, model);
        mappedList = Arrays.asList(modelDto, modelDto, modelDto);
    }

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(mapper, repository);
    }

    @Test
    void save() {
        Pencil mapped = mock(Pencil.class);
        Pencil saved = mock(Pencil.class);
        PencilDto countryDtoReturn = mock(PencilDto.class);
        PencilDto startDto = mock(PencilDto.class);

        when(mapper.mapFromDto(any(PencilDto.class))).thenReturn(mapped);
        when(repository.save(any(Pencil.class))).thenReturn(saved);
        when(mapper.mapFromModel(any(Pencil.class))).thenReturn(countryDtoReturn);

        PencilDto result = service.save(startDto);

        assertEquals(result, countryDtoReturn);

        verify(repository).findByNameAndProducerId(startDto.getName(), startDto.getProducerId());
        verify(mapper).mapFromDto(startDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(saved);
    }

    @Test
    void saveAndUpdateWithException() {
        String pencilName = "Test pencil name";
        when(repository.findByNameAndProducerId(anyString(), anyInt())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> repository.findByNameAndProducerId(pencilName, ID));

        verify(repository).findByNameAndProducerId(pencilName, ID);
    }

    @Test
    void findById() {
        Pencil model = mock(Pencil.class);
        PencilDto modelDto = mock(PencilDto.class);
        Optional<Pencil> optionalModel = Optional.of(model);

        when(repository.findById(anyInt())).thenReturn(optionalModel);
        when(mapper.mapFromModel(any(Pencil.class))).thenReturn(modelDto);

        PencilDto result = service.findById(ID);
        assertEquals(result, modelDto);

        verify(repository).findById(ID);
        verify(mapper).mapFromModel(model);
    }

    @Test
    void findByIdWithException() {
        assertThrows(EntityNotFoundException.class, () -> service.findById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void findAll() {
        PencilSearchCriteriaDto search = mock(PencilSearchCriteriaDto.class);

        when(repository.findAllWithSearchCriteriaDto(any(PencilSearchCriteriaDto.class))).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        List<PencilDto> result = service.findAll(search);
        assertArrayEquals(result.toArray(), mappedList.toArray());

        verify(repository).findAllWithSearchCriteriaDto(search);
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void update() {
        Pencil mapped = mock(Pencil.class);
        Pencil updated = mock(Pencil.class);
        PencilDto dtoReturned = mock(PencilDto.class);
        PencilDto startDto = mock(PencilDto.class);

        when(mapper.mapFromDto(any(PencilDto.class))).thenReturn(mapped);
        when(repository.save(any(Pencil.class))).thenReturn(updated);
        when(mapper.mapFromModel(any(Pencil.class))).thenReturn(dtoReturned);

        PencilDto result = service.update(ID, startDto);

        assertEquals(result, dtoReturned);

        verify(repository).findByNameAndProducerId(startDto.getName(), startDto.getProducerId());
        verify(mapper).mapFromDto(startDto);
        verify(repository).save(mapped);
        verify(mapper).mapFromModel(updated);
    }

    @Test
    void deleteById() {
        Pencil model = mock(Pencil.class);
        Optional<Pencil> optionalModel = Optional.of(model);

        when(repository.findById(anyInt())).thenReturn(optionalModel);

        service.deleteById(ID);

        verify(repository).findById(ID);
        verify(repository).deleteById(ID);
    }

    @Test
    void deleteByIdWithException() {
        assertThrows(EntityNotFoundException.class, () -> service.deleteById(ID));

        verify(repository).findById(ID);
    }

    @Test
    void calculateTheBasket() {
        BasketPencilDto data = new BasketPencilDto()
                .setPencilDtoList(Arrays.asList(
                        new ItemPencilDto().setId(1).setQuantity(2),
                        new ItemPencilDto().setId(2).setQuantity(3)));

        when(repository.findById(anyInt())).thenReturn(Optional.of(new Pencil().setPriceInCents(10)));

        assertEquals(service.calculateTheBasket(data), 50);

        verify(repository, times(2)).findById(anyInt());
    }

    @Test
    void findByPriceInCentsBetween() {
        when(repository.findByPriceInCentsBetweenOrderByIdDesc(anyInt(), anyInt())).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        assertArrayEquals(service.findByPriceInCentsBetween(100, 200).toArray(),
                mappedList.toArray());

        verify(repository).findByPriceInCentsBetweenOrderByIdDesc(100, 200);
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void findByHardnessAndEraser() {
        when(repository.findByHardnessAndEraser(any(PencilHardness.class), anyBoolean())).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        assertArrayEquals(service.findByHardnessAndEraser(PencilHardness.H, true).toArray(),
                mappedList.toArray());

        verify(repository).findByHardnessAndEraser(PencilHardness.H, true);
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void findByNameStartingWithIgnoreCase() {
        when(repository.findByNameStartingWithIgnoreCase(anyString())).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        assertArrayEquals(service.findByNameStartingWithIgnoreCase("").toArray(),
                mappedList.toArray());

        verify(repository).findByNameStartingWithIgnoreCase("");
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void findByProductionDateBefore() {
        when(repository.findByProductionDateBefore(any(Instant.class))).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        assertArrayEquals(service.findByProductionDateBefore(Instant.parse("2001-01-01T00:00:00Z")).toArray(),
                mappedList.toArray());

        verify(repository).findByProductionDateBefore(Instant.parse("2001-01-01T00:00:00Z"));
        verify(mapper).mapListFromModel(modelList);
    }

    @Test
    void findByCountryNameContainingOrProducerNameContaining() {
        when(repository.findByCountryNameContainingOrProducerNameContaining(anyString(), anyString())).thenReturn(modelList);
        when(mapper.mapListFromModel(anyList())).thenReturn(mappedList);

        assertArrayEquals(service.findByCountryNameContainingOrProducerNameContaining("A", "B").toArray(),
                mappedList.toArray());

        verify(repository).findByCountryNameContainingOrProducerNameContaining("A", "B");
        verify(mapper).mapListFromModel(modelList);
    }
}