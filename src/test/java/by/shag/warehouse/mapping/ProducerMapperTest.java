package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.ProducerDto;
import by.shag.warehouse.jpa.model.Producer;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;

class ProducerMapperTest {

    private final Integer ID = 1;
    private final String NAME = "Test_Name";
    private final ProducerMapperImpl mapperImpl = new ProducerMapperImpl();

    @Test
    void mapFromDto() {
        Producer result = mapperImpl.mapFromDto(generateProducerDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapFromModel() {
        ProducerDto result = mapperImpl.mapFromModel(generateProducer());

        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapListFromModel() {
        List<Producer> modelList = Arrays.asList(generateProducer(), generateProducer(), generateProducer().setName("ARG"));

        List<ProducerDto> result = mapperImpl.mapListFromModel(modelList);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is("ARG"))
                ))
        ));
    }

    @Test
    void mapListFromDto() {
        List<ProducerDto> modelDtoList = Arrays.asList(generateProducerDto(), generateProducerDto(), generateProducerDto().setName("USA"));

        List<Producer> result = mapperImpl.mapListFromDto(modelDtoList);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is("USA"))
                ))
        ));
    }

    private ProducerDto generateProducerDto() {
        return new ProducerDto()
                .setId(ID)
                .setName(NAME);
    }

    private Producer generateProducer() {
        return new Producer()
                .setId(ID)
                .setName(NAME);
    }
}