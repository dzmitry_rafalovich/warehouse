package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.PuncherDto;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.jpa.model.Puncher;
import by.shag.warehouse.jpa.model.PuncherKnives;
import by.shag.warehouse.service.CountryService;
import by.shag.warehouse.service.ProducerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PuncherMapperTest {

    private static final Integer ID = 1;
    private static final String NAME = "MyPuncher";
    private static final Integer PRICE_IN_CENTS = 100;
    private static final Instant PRODUCTION_DAY = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 12;
    private static final Integer COUNTRY_ID = 2;
    private static final String COUNTRY = "MyCountry";
    private static final Integer PRODUCER_ID = 3;
    private static final String PRODUCER = "MyProducer";
    private static final PuncherKnives PUNCHER_KNIVES = PuncherKnives.ONE;
    private static final Integer MAX_SHEETS = 20;
    private static final Boolean DISCOUNT = true;

    @InjectMocks
    private PuncherMapperImpl mapper;
    @Mock
    private CountryService countryService;
    @Mock
    private ProducerService producerService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(countryService, producerService);
    }

    @Test
    void mapFromDto() {

        Country country = generateCountry();
        Producer producer = generateProducer();

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(country);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(producer);

        Puncher result = mapper.mapFromDto(generatePuncherDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENTS);
        assertEquals(result.getProductionDate(), PRODUCTION_DAY);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountry(), country);
        assertEquals(result.getProducer(), producer);
        assertEquals(result.getPuncherKnives(), PUNCHER_KNIVES);
        assertEquals(result.getMaxSheets(), MAX_SHEETS);
        assertEquals(result.getDiscount(), DISCOUNT);

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    @Test
    void mapFromDtoWithNoCountry() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generatePuncherDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
    }

    @Test
    void mapFromDtoWithNoProducer() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(generateCountry());
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generatePuncherDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    @Test
    void mapFromModel() {
        PuncherDto result = mapper.mapFromModel(generatePuncher());

        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENTS);
        assertEquals(result.getProductionDate(), PRODUCTION_DAY);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountryId(), COUNTRY_ID);
        assertEquals(result.getProducerId(), PRODUCER_ID);
        assertEquals(result.getPuncherKnives(), PUNCHER_KNIVES);
        assertEquals(result.getMaxSheets(), MAX_SHEETS);
        assertEquals(result.getDiscount(), DISCOUNT);
    }

    @Test
    void mapListFromModel() {
        List<Puncher> modelList = Arrays.asList(generatePuncher(), generatePuncher());

        List<PuncherDto> result = mapper.mapListFromModel(modelList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY_ID)),
                        hasProperty("producerId", is(PRODUCER_ID)),
                        hasProperty("puncherKnives", is(PUNCHER_KNIVES)),
                        hasProperty("maxSheets", is(MAX_SHEETS)),
                        hasProperty("discount", is(DISCOUNT))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY_ID)),
                        hasProperty("producerId", is(PRODUCER_ID)),
                        hasProperty("puncherKnives", is(PUNCHER_KNIVES)),
                        hasProperty("maxSheets", is(MAX_SHEETS)),
                        hasProperty("discount", is(DISCOUNT))
                ))
        ));
    }

    @Test
    void mapListFromDto() {
        Country country = generateCountry();
        Producer producer = generateProducer();

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(country);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(producer);

        List<PuncherDto> dtoList = Arrays.asList(generatePuncherDto(), generatePuncherDto());

        List<Puncher> result = mapper.mapListFromDto(dtoList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(country)),
                        hasProperty("producer", is(producer)),
                        hasProperty("puncherKnives", is(PUNCHER_KNIVES)),
                        hasProperty("maxSheets", is(MAX_SHEETS)),
                        hasProperty("discount", is(DISCOUNT))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(country)),
                        hasProperty("producer", is(producer)),
                        hasProperty("puncherKnives", is(PUNCHER_KNIVES)),
                        hasProperty("maxSheets", is(MAX_SHEETS)),
                        hasProperty("discount", is(DISCOUNT))
                ))
        ));

        verify(countryService, times(2)).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService, times(2)).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    private PuncherDto generatePuncherDto() {
        return new PuncherDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY_ID)
                .setProducerId(PRODUCER_ID)
                .setPuncherKnives(PUNCHER_KNIVES)
                .setMaxSheets(MAX_SHEETS)
                .setDiscount(DISCOUNT);
    }

    private Puncher generatePuncher() {
        return new Puncher()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountry(generateCountry())
                .setProducer(generateProducer())
                .setPuncherKnives(PUNCHER_KNIVES)
                .setMaxSheets(MAX_SHEETS)
                .setDiscount(DISCOUNT);
    }

    private Producer generateProducer() {
        return new Producer()
                .setId(PRODUCER_ID)
                .setName(PRODUCER);
    }

    private Country generateCountry() {
        return new Country()
                .setId(COUNTRY_ID)
                .setName(COUNTRY);
    }
}