package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.CountryDto;
import by.shag.warehouse.jpa.model.Country;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.jupiter.api.Assertions.*;

class CountryMapperTest {

    private static final Integer ID = 1;
    private static final String NAME = "Test_Name";
    private final CountryMapperImpl mapperImpl = new CountryMapperImpl();


    @Test
    void mapFromDto() {
        Country result = mapperImpl.mapFromDto(generateCountryDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapFromModel() {
        CountryDto result = mapperImpl.mapFromModel(generateCountry());
        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
    }

    @Test
    void mapListFromModel() {
        List<Country> modelList = Arrays.asList(generateCountry(), generateCountry().setName("BLR"), generateCountry());

        List<CountryDto> result = mapperImpl.mapListFromModel(modelList);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is("BLR"))
                ))
        ));
    }

    @Test
    void mapListFromDto() {

        List<CountryDto> modelDtoList = Arrays.asList(generateCountryDto(), generateCountryDto().setName("RUS"));

        List<Country> result = mapperImpl.mapListFromDto(modelDtoList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is("RUS"))
                ))
        ));
    }

    private Country generateCountry() {
        return new Country()
                .setId(ID)
                .setName(NAME);
    }

    private CountryDto generateCountryDto() {
        return new CountryDto()
                .setId(ID)
                .setName(NAME);
    }
}