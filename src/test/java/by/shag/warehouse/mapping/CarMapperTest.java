package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.CarDto;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.*;
import by.shag.warehouse.service.CountryService;
import by.shag.warehouse.service.ProducerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class CarMapperTest {

    private static final Country COUNTRY = new Country().setId(1).setName("Беларусь");
    private static final Producer PRODUCER = new Producer().setId(2).setName("Живет");

    private static final Integer ID = 1;
    private static final String NAME = "Bugatti";
    private static final Integer PRICE_IN_CENT = 560000;
    private static final Instant PRODUCTION_DATE = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 7;
    private static final Boolean ELECTRICAL = true;
    private static final CarsCarcase CARCASE = CarsCarcase.SEDAN;

    @InjectMocks
    private CarMapperImpl mapper;
    @Mock
    private CountryService countryService;
    @Mock
    private ProducerService producerService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(countryService, producerService);
    }

    @Test
    void mapFromDto() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(PRODUCER);

        Car result = mapper.mapFromDto(generateModelDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENT);
        assertEquals(result.getProductionDate(), PRODUCTION_DATE);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountry(), COUNTRY);
        assertEquals(result.getProducer(), PRODUCER);
        assertEquals(result.getElectrical(), ELECTRICAL);
        assertEquals(result.getCarcase(), CARCASE);

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER.getId());
    }

    @Test
    void mapFromDtoWithNoCountry() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);
        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateModelDto()));
        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
    }

    @Test
    void mapFromDtoWithNoProducer() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);
        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateModelDto()));
        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER.getId());
    }


    @Test
    void mapFromModel() {
        CarDto result = mapper.mapFromModel(generateModel());

        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENT);
        assertEquals(result.getProductionDate(), PRODUCTION_DATE);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountryId(), COUNTRY.getId());
        assertEquals(result.getProducerId(), PRODUCER.getId());
        assertEquals(result.getElectrical(), ELECTRICAL);
        assertEquals(result.getCarcase(), CARCASE);
    }

    @Test
    void mapListFromModel() {
        List<Car> modelList = Arrays.asList(generateModel(), generateModel());

        List<CarDto> result = mapper.mapListFromModel(modelList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("productionDate", is(PRODUCTION_DATE)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY.getId())),
                        hasProperty("producerId", is(PRODUCER.getId())),
                        hasProperty("electrical", is(ELECTRICAL)),
                        hasProperty("carcase", is(CARCASE))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("productionDate", is(PRODUCTION_DATE)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY.getId())),
                        hasProperty("producerId", is(PRODUCER.getId())),
                        hasProperty("electrical", is(ELECTRICAL)),
                        hasProperty("carcase", is(CARCASE))
                ))
        ));
    }

    @Test
    void mapListFromDto() {
        List<CarDto> modelDtoList = Arrays.asList(generateModelDto(), generateModelDto());

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(PRODUCER);

        List<Car> result = mapper.mapListFromDto(modelDtoList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("productionDate", is(PRODUCTION_DATE)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(COUNTRY)),
                        hasProperty("producer", is(PRODUCER)),
                        hasProperty("electrical", is(ELECTRICAL)),
                        hasProperty("carcase", is(CARCASE))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("productionDate", is(PRODUCTION_DATE)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(COUNTRY)),
                        hasProperty("producer", is(PRODUCER)),
                        hasProperty("electrical", is(ELECTRICAL)),
                        hasProperty("carcase", is(CARCASE))
                ))
        ));

        verify(countryService, times(2)).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService, times(2)).findByIdWhichWillReturnModel(PRODUCER.getId());
    }

    private CarDto generateModelDto() {
        return new CarDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENT)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setElectrical(ELECTRICAL)
                .setCarcase(CARCASE);
    }

    private Car generateModel() {
        return new Car()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENT)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountry(COUNTRY)
                .setProducer(PRODUCER)
                .setElectrical(ELECTRICAL)
                .setCarcase(CARCASE);
    }
}