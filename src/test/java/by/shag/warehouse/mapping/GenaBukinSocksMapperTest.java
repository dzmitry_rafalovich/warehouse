package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.GenaBukinSocksDto;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import by.shag.warehouse.jpa.model.PowerSmell;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.service.CountryService;
import by.shag.warehouse.service.ProducerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class GenaBukinSocksMapperTest {

    private static final Integer COUNTRY_ID = 1;
    private static final String COUNTRY_NAME = "Bell";
    private static final Integer PRODUCER_ID = 2;
    private static final String PRODUCER_NAME = "BellTriKoTaGH";
    private static final Integer ID = 3;
    private static final String NAME = "Vlad";
    private static final Integer PRICE_IN_CENT = 4;
    private static final Instant PRODUCTION_DATE = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 5;
    private static final Integer HOLES_QUANTITY = 6;
    private static final PowerSmell POWER_SMELL = PowerSmell.DEADLY;
    private static final String FABRIC = "HB";

    @InjectMocks
    private GenaBukinSocksMapperImpl mapper;
    @Mock
    private CountryService countryService;
    @Mock
    private ProducerService producerService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(countryService, producerService);
    }

    @Test
    void mapFromDto() {
        Country country = generateCountry();
        Producer producer = generateProducer();

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(country);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(producer);

        GenaBukinSocks result = mapper.mapFromDto(generateGenaBukinSocksDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENT);
        assertEquals(result.getProductionDate(), PRODUCTION_DATE);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getHolesQuantity(), HOLES_QUANTITY);
        assertEquals(result.getPowerSmell(), POWER_SMELL);
        assertEquals(result.getCountry(), country);
        assertEquals(result.getProducer(), producer);
        assertEquals(result.getFabric(), FABRIC);

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    @Test
    void mapFromDtoWithNoCountry() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateGenaBukinSocksDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
    }

    @Test
    void mapFromDtoWithNoProducer() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(generateCountry());
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateGenaBukinSocksDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    @Test
    void mapFromModel() {
        GenaBukinSocksDto result = mapper.mapFromModel(generateGenaBukinSocksModel());

        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENT);
        assertEquals(result.getProductionDate(), PRODUCTION_DATE);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountryId(), COUNTRY_ID);
        assertEquals(result.getProducerId(), PRODUCER_ID);
        assertEquals(result.getHolesQuantity(), HOLES_QUANTITY);
        assertEquals(result.getPowerSmell(), POWER_SMELL);
        assertEquals(result.getFabric(), FABRIC);
    }

    @Test
    void mapListFromModel() {
        List<GenaBukinSocks> modeList = Arrays.asList(
                generateGenaBukinSocksModel(), generateGenaBukinSocksModel(), generateGenaBukinSocksModel());

        List<GenaBukinSocksDto> result = mapper.mapListFromModel(modeList);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY_ID)),
                        hasProperty("producerId", is(PRODUCER_ID)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY_ID)),
                        hasProperty("producerId", is(PRODUCER_ID)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY_ID)),
                        hasProperty("producerId", is(PRODUCER_ID)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                ))
        ));
    }

    @Test
    void mapListFromDto() {
        Country country = generateCountry();
        Producer producer = generateProducer();

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(country);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(producer);

        List<GenaBukinSocksDto> dtoList = Arrays.asList(
                generateGenaBukinSocksDto(), generateGenaBukinSocksDto(), generateGenaBukinSocksDto());

        List<GenaBukinSocks> result = mapper.mapListFromDto(dtoList);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(country)),
                        hasProperty("producer", is(producer)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(country)),
                        hasProperty("producer", is(producer)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENT)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(country)),
                        hasProperty("producer", is(producer)),
                        hasProperty("holesQuantity", is(HOLES_QUANTITY)),
                        hasProperty("powerSmell", is(POWER_SMELL)),
                        hasProperty("fabric", is(FABRIC))
                ))
        ));

        verify(countryService, times(3)).findByIdWhichWillReturnModel(COUNTRY_ID);
        verify(producerService, times(3)).findByIdWhichWillReturnModel(PRODUCER_ID);
    }

    private GenaBukinSocksDto generateGenaBukinSocksDto() {
        return new GenaBukinSocksDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENT)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY_ID)
                .setProducerId(PRODUCER_ID)
                .setHolesQuantity(HOLES_QUANTITY)
                .setPowerSmell(POWER_SMELL)
                .setFabric(FABRIC);
    }

    private GenaBukinSocks generateGenaBukinSocksModel() {
        return new GenaBukinSocks()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENT)
                .setProductionDate(PRODUCTION_DATE)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountry(generateCountry())
                .setProducer(generateProducer())
                .setHolesQuantity(HOLES_QUANTITY)
                .setPowerSmell(POWER_SMELL)
                .setFabric(FABRIC);
    }

    private Producer generateProducer() {
        return new Producer()
                .setId(PRODUCER_ID)
                .setName(PRODUCER_NAME);
    }

    private Country generateCountry() {
        return new Country()
                .setId(COUNTRY_ID)
                .setName(COUNTRY_NAME);
    }
}