package by.shag.warehouse.mapping;

import by.shag.warehouse.api.dto.PencilDto;
import by.shag.warehouse.exception.EntityServiceException;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import by.shag.warehouse.jpa.model.Producer;
import by.shag.warehouse.service.CountryService;
import by.shag.warehouse.service.ProducerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.WARN)
class PencilMapperTest {

    private static final Integer ID = 1;
    private static final String NAME = "Test_Name";
    private static final Integer PRICE_IN_CENTS = 100;
    private static final Instant PRODUCTION_DAY = Instant.now();
    private static final Integer GUARANTY_IN_MONTHS = 10;
    private static final Country COUNTRY = new Country().setId(2).setName("Test Country Name");
    private static final Producer PRODUCER = new Producer().setId(3).setName("Test Producer Name");
    private static final Boolean ERASER = true;
    private static final PencilHardness HARDNESS = PencilHardness.B;
    private static final String COLOR = "Test_Color";

    @InjectMocks
    private PencilMapperImpl mapper;
    @Mock
    private CountryService countryService;
    @Mock
    private ProducerService producerService;

    @AfterEach
    void tearDown() {
        verifyNoMoreInteractions(countryService, producerService);
    }

    @Test
    void mapFromDto() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(PRODUCER);

        Pencil result = mapper.mapFromDto(generateModelDto());

        assertNull(result.getId());
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENTS);
        assertEquals(result.getProductionDate(), PRODUCTION_DAY);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountry(), COUNTRY);
        assertEquals(result.getProducer(), PRODUCER);
        assertEquals(result.getEraser(), ERASER);
        assertEquals(result.getHardness(), HARDNESS);
        assertEquals(result.getColor(), COLOR);

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER.getId());
    }

    @Test
    void mapFromDtoWithNoCountry() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateModelDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
    }

    @Test
    void mapFromDtoWithNoProducer() {
        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenThrow(EntityServiceException.class);

        assertThrows(EntityServiceException.class, () -> mapper.mapFromDto(generateModelDto()));

        verify(countryService).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService).findByIdWhichWillReturnModel(PRODUCER.getId());
    }

    @Test
    void mapFromModel() {
        PencilDto result = mapper.mapFromModel(generateModel());

        assertEquals(result.getId(), ID);
        assertEquals(result.getName(), NAME);
        assertEquals(result.getPriceInCents(), PRICE_IN_CENTS);
        assertEquals(result.getProductionDate(), PRODUCTION_DAY);
        assertEquals(result.getGuarantyInMonths(), GUARANTY_IN_MONTHS);
        assertEquals(result.getCountryId(), COUNTRY.getId());
        assertEquals(result.getProducerId(), PRODUCER.getId());
        assertEquals(result.getEraser(), ERASER);
        assertEquals(result.getHardness(), HARDNESS);
        assertEquals(result.getColor(), COLOR);
    }

    @Test
    void mapListFromModel() {
        List<Pencil> modelList = Arrays.asList(generateModel(), generateModel());

        List<PencilDto> result = mapper.mapListFromModel(modelList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY.getId())),
                        hasProperty("producerId", is(PRODUCER.getId())),
                        hasProperty("eraser", is(ERASER)),
                        hasProperty("hardness", is(HARDNESS)),
                        hasProperty("color", is(COLOR))
                )),
                hasItem(allOf(
                        hasProperty("id", is(ID)),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("countryId", is(COUNTRY.getId())),
                        hasProperty("producerId", is(PRODUCER.getId())),
                        hasProperty("eraser", is(ERASER)),
                        hasProperty("hardness", is(HARDNESS)),
                        hasProperty("color", is(COLOR))
                ))
        ));
    }

    @Test
    void mapListFromDto() {
        List<PencilDto> modelDtoList = Arrays.asList(generateModelDto(), generateModelDto());

        when(countryService.findByIdWhichWillReturnModel(anyInt())).thenReturn(COUNTRY);
        when(producerService.findByIdWhichWillReturnModel(anyInt())).thenReturn(PRODUCER);

        List<Pencil> result = mapper.mapListFromDto(modelDtoList);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(COUNTRY)),
                        hasProperty("producer", is(PRODUCER)),
                        hasProperty("eraser", is(ERASER)),
                        hasProperty("hardness", is(HARDNESS)),
                        hasProperty("color", is(COLOR))
                )),
                hasItem(allOf(
                        hasProperty("id", nullValue()),
                        hasProperty("name", is(NAME)),
                        hasProperty("priceInCents", is(PRICE_IN_CENTS)),
                        hasProperty("productionDate", is(PRODUCTION_DAY)),
                        hasProperty("guarantyInMonths", is(GUARANTY_IN_MONTHS)),
                        hasProperty("country", is(COUNTRY)),
                        hasProperty("producer", is(PRODUCER)),
                        hasProperty("eraser", is(ERASER)),
                        hasProperty("hardness", is(HARDNESS)),
                        hasProperty("color", is(COLOR))
                ))
        ));

        verify(countryService, times(2)).findByIdWhichWillReturnModel(COUNTRY.getId());
        verify(producerService, times(2)).findByIdWhichWillReturnModel(PRODUCER.getId());
    }

    private PencilDto generateModelDto() {
        return new PencilDto()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountryId(COUNTRY.getId())
                .setProducerId(PRODUCER.getId())
                .setEraser(ERASER)
                .setHardness(HARDNESS)
                .setColor(COLOR);
    }

    private Pencil generateModel() {
        return new Pencil()
                .setId(ID)
                .setName(NAME)
                .setPriceInCents(PRICE_IN_CENTS)
                .setProductionDate(PRODUCTION_DAY)
                .setGuarantyInMonths(GUARANTY_IN_MONTHS)
                .setCountry(COUNTRY)
                .setProducer(PRODUCER)
                .setEraser(ERASER)
                .setHardness(HARDNESS)
                .setColor(COLOR);
    }

}