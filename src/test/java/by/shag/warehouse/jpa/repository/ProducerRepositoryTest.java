package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.config.PostgresTestConfiguration;
import by.shag.warehouse.jpa.model.Producer;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;

@DataJpaTest
@ComponentScan("by.shag.warehouse.jpa.repository")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ImportAutoConfiguration(PostgresTestConfiguration.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class})
@DatabaseSetup({
        "classpath:dbunit/producers.xml"
})
class ProducerRepositoryTest {

    @Autowired
    private ProducerRepository repository;

    @Test
    void findByName() {
        Producer result = repository.findByName("BELAZ").get();
        assertThat("Wrong id", result.getId(), is(1002));
    }

}