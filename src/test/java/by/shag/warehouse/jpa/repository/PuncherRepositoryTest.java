package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.PuncherSearchCriteriaDto;
import by.shag.warehouse.config.PostgresTestConfiguration;
import by.shag.warehouse.jpa.model.Puncher;
import by.shag.warehouse.jpa.model.PuncherKnives;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ImportAutoConfiguration(PostgresTestConfiguration.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class
})
@DatabaseSetup({
        "classpath:dbunit/countries.xml",
        "classpath:dbunit/producers.xml",
        "classpath:dbunit/punchers.xml"
})
class PuncherRepositoryTest {
    @Autowired
    private PuncherRepository puncherRepository;

    @Test
    void save() {

    }

    @Test
    void findByID() {
        Puncher result = puncherRepository.findById(1001).get();
        assertThat("Wrong name", result.getName(), is("Puncher0"));
        assertThat("Wrong price In Cents",
                result.getPriceInCents(), is(100));
        assertThat("Wrong production Date",
                result.getProductionDate(), is(Instant.parse("2021-09-12T00:00:00.0Z")));
        assertThat("Wrong guaranty In Months",
                result.getGuarantyInMonths(), is(12));
        assertThat("Wrong country id",
                result.getCountry().getId(), is(1001));
        assertThat("Wrong country name",
                result.getCountry().getName(), is("BELARUS"));
        assertThat("Wrong producer id",
                result.getProducer().getId(), is(1001));
        assertThat("Wrong puncher knives",
                result.getPuncherKnives(), is(PuncherKnives.TWO));
        assertThat("Wrong puncher number of sheets",
                result.getMaxSheets(), is(15));
        assertThat("Wrong puncher discount",
                result.getDiscount(), is(true));
    }

    @Test
    void findAll() {
        PuncherSearchCriteriaDto searchCriteriaDto = new PuncherSearchCriteriaDto();
        List<Puncher> result = puncherRepository.findAll(searchCriteriaDto);

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("Puncher0")),
                        hasProperty("priceInCents", is(100)),
                        hasProperty("productionDate", is(Instant.parse("2021-09-12T00:00:00.0Z"))),
                        hasProperty("guarantyInMonths", is(12)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("puncherKnives", is(PuncherKnives.TWO)),
                        hasProperty("maxSheets", is(15)),
                        hasProperty("discount", is(true))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1002)),
                        hasProperty("name", is("Puncher1")),
                        hasProperty("priceInCents", is(10)),
                        hasProperty("productionDate", is(Instant.parse("2021-09-12T00:00:00.0Z"))),
                        hasProperty("guarantyInMonths", is(12)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("puncherKnives", is(PuncherKnives.ONE)),
                        hasProperty("maxSheets", is(15)),
                        hasProperty("discount", is(true))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Puncher2")),
                        hasProperty("priceInCents", is(200)),
                        hasProperty("productionDate", is(Instant.parse("2021-09-12T00:00:00.0Z"))),
                        hasProperty("guarantyInMonths", is(12)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1002)), hasProperty("name", is("RUSSIA"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("puncherKnives", is(PuncherKnives.ONE)),
                        hasProperty("maxSheets", is(88)),
                        hasProperty("discount", is(true))
                ))
        ));
    }
}