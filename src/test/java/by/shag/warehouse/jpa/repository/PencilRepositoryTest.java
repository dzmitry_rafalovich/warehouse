package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.PencilSearchCriteriaDto;
import by.shag.warehouse.config.PostgresTestConfiguration;
import by.shag.warehouse.jpa.model.Pencil;
import by.shag.warehouse.jpa.model.PencilHardness;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ImportAutoConfiguration(PostgresTestConfiguration.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class})
@DatabaseSetup({
        "classpath:dbunit/countries.xml",
        "classpath:dbunit/producers.xml",
        "classpath:dbunit/pencils.xml"})
class PencilRepositoryTest {

    @Autowired
    private PencilRepository repository;

    @Test
    void findByNameAndProducerId() {
        Pencil result = repository.findByNameAndProducerId("Pencil two", 1002).get();

        assertThat("Wrong name", result.getName(), is("Pencil two"));
        assertThat("Wrong price_in_cents", result.getPriceInCents(), is(200));
        assertThat("Wrong guaranty_in_months", result.getGuarantyInMonths(), is(2));
        assertThat("Wrong country_id", result.getCountry().getId(), is(1002));
        assertThat("Wrong country_name", result.getCountry().getName(), is("RUSSIA"));
        assertThat("Wrong producer_id", result.getProducer().getId(), is(1002));
        assertThat("Wrong producer_name", result.getProducer().getName(), is("BELAZ"));
        assertThat("Wrong eraser", result.getEraser(), is(false));
        assertThat("Wrong hardness", result.getHardness(), is(PencilHardness.HB));
        assertThat("Wrong color", result.getColor(), is("white"));
    }

    @Test
    @Disabled
    void findAllWithCriteria() {
        PencilSearchCriteriaDto searchCriteria = new PencilSearchCriteriaDto().setMinPrice(100).setColor("black");

        List<Pencil> result = repository.findAllWithSearchCriteriaDto(searchCriteria);

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("Pencil one")),
                        hasProperty("priceInCents", is(100)),
                        hasProperty("guarantyInMonths", is(1)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("eraser", is(true)),
                        hasProperty("hardness", is(PencilHardness.H)),
                        hasProperty("color", is("black"))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Pencil ot")),
                        hasProperty("priceInCents", is(300)),
                        hasProperty("guarantyInMonths", is(3)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("UKRAINE"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("HORIZONT"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.B)),
                        hasProperty("color", is("black"))
                ))
        ));
    }

    @Test
    void findByPriceInCentsBetweenOrderByIdDesc() {
        List<Pencil> result = repository.findByPriceInCentsBetweenOrderByIdDesc(150, 250);

        assertThat("Wrong result size", result, hasSize(1));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1002)),
                        hasProperty("name", is("Pencil two")),
                        hasProperty("priceInCents", is(200)),
                        hasProperty("guarantyInMonths", is(2)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1002)), hasProperty("name", is("RUSSIA"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1002)), hasProperty("name", is("BELAZ"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.HB)),
                        hasProperty("color", is("white"))
                ))
        ));
    }

    @Test
    void findByHardnessAndEraser() {
        List<Pencil> result = repository.findByHardnessAndEraser(PencilHardness.B, false);

        assertThat("Wrong result size", result, hasSize(1));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Pencil ot")),
                        hasProperty("priceInCents", is(300)),
                        hasProperty("guarantyInMonths", is(3)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("UKRAINE"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("HORIZONT"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.B)),
                        hasProperty("color", is("black"))
                ))
        ));
    }

    @Test
    void findByNameStartingWithIgnoreCase() {
        List<Pencil> result = repository.findByNameStartingWithIgnoreCase("pENCIL O");

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("Pencil one")),
                        hasProperty("priceInCents", is(100)),
                        hasProperty("guarantyInMonths", is(1)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("eraser", is(true)),
                        hasProperty("hardness", is(PencilHardness.H)),
                        hasProperty("color", is("black"))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Pencil ot")),
                        hasProperty("priceInCents", is(300)),
                        hasProperty("guarantyInMonths", is(3)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("UKRAINE"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("HORIZONT"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.B)),
                        hasProperty("color", is("black"))
                ))
        ));
    }

    @Test
    void findByProductionDateBefore() {
        List<Pencil> result = repository.findByProductionDateBefore(Instant.now().truncatedTo(ChronoUnit.SECONDS));

        assertThat("Wrong result size", result, hasSize(3));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("Pencil one")),
                        hasProperty("priceInCents", is(100)),
                        hasProperty("guarantyInMonths", is(1)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("eraser", is(true)),
                        hasProperty("hardness", is(PencilHardness.H)),
                        hasProperty("color", is("black"))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1002)),
                        hasProperty("name", is("Pencil two")),
                        hasProperty("priceInCents", is(200)),
                        hasProperty("guarantyInMonths", is(2)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1002)), hasProperty("name", is("RUSSIA"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1002)), hasProperty("name", is("BELAZ"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.HB)),
                        hasProperty("color", is("white"))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Pencil ot")),
                        hasProperty("priceInCents", is(300)),
                        hasProperty("guarantyInMonths", is(3)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("UKRAINE"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("HORIZONT"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.B)),
                        hasProperty("color", is("black"))
                ))
        ));
    }

    @Test
    void findByCountryNameContainingOrProducerNameContaining() {
        List<Pencil> result = repository.findByCountryNameContainingOrProducerNameContaining("B", "H");

        assertThat("Wrong result size", result, hasSize(2));
        assertThat("Wrong item", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("Pencil one")),
                        hasProperty("priceInCents", is(100)),
                        hasProperty("guarantyInMonths", is(1)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELARUS"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1001)), hasProperty("name", is("BELITA"))))),
                        hasProperty("eraser", is(true)),
                        hasProperty("hardness", is(PencilHardness.H)),
                        hasProperty("color", is("black"))
                )),
                hasItem(allOf(
                        hasProperty("id", is(1003)),
                        hasProperty("name", is("Pencil ot")),
                        hasProperty("priceInCents", is(300)),
                        hasProperty("guarantyInMonths", is(3)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("UKRAINE"))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003)), hasProperty("name", is("HORIZONT"))))),
                        hasProperty("eraser", is(false)),
                        hasProperty("hardness", is(PencilHardness.B)),
                        hasProperty("color", is("black"))
                ))
        ));
    }

}