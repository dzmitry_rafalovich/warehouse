package by.shag.warehouse.jpa.repository;

import by.shag.warehouse.api.dto.GenaBukinSocksCriteriaDto;
import by.shag.warehouse.config.PostgresTestConfiguration;
import by.shag.warehouse.jpa.model.Country;
import by.shag.warehouse.jpa.model.GenaBukinSocks;
import by.shag.warehouse.jpa.model.PowerSmell;
import by.shag.warehouse.jpa.model.Producer;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import com.github.springtestdbunit.assertion.DatabaseAssertionMode;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.time.Instant;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@DataJpaTest
@ComponentScan("by.shag.warehouse.jpa.repository")
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
@ImportAutoConfiguration(PostgresTestConfiguration.class)
@TestExecutionListeners({
        DependencyInjectionTestExecutionListener.class,
        TransactionDbUnitTestExecutionListener.class})
@DatabaseSetup({
        "classpath:dbunit/countries.xml",
        "classpath:dbunit/producers.xml",
        "classpath:dbunit/socks.xml"
})
class GenaBukinSocksRepositoryTest {

    private static final Integer ID = 1001;

    @Autowired
    private GenaBukinSocksRepository repository;

    @Disabled
    @ExpectedDatabase(table = "socks", value = "classpath:dbunit/expectation/socks-save.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    @Test
    void save() {
        GenaBukinSocks socks = new GenaBukinSocks()
                .setName("CoolSocks")
                .setPriceInCents(220)
                .setProductionDate(Instant.parse("2012-11-24T11:10:00Z"))
                .setGuarantyInMonths(5)
                .setCountry(new Country()
                        .setId(1003)
                        .setName("UKRAINE"))
                .setProducer(new Producer()
                        .setId(1002)
                        .setName("BELAZ"))
                .setHolesQuantity(15)
                .setPowerSmell(PowerSmell.DEADLY)
                .setFabric("HB");

        repository.save(socks);
    }

    @Test
    void findByID() {
        GenaBukinSocks socks = repository.findById(ID).get();

        assertThat("Wrong name", socks.getName(), is("JustSocks"));
        assertThat("Wrong price", socks.getPriceInCents(), is(60));
        assertThat("Wrong date", socks.getProductionDate().toString(), is("2019-11-24T11:10:00Z"));
        assertThat("Wrong guaranty", socks.getGuarantyInMonths(), is(4));
        assertThat("Wrong country", socks.getCountry().getId(), is(1002));
        assertThat("Wrong producer", socks.getProducer().getId(), is(1003));
        assertThat("Wrong holes", socks.getHolesQuantity(), is(13));
        assertThat("Wrong smell", socks.getPowerSmell(), is(PowerSmell.DEADLY));
        assertThat("Wrong fabric", socks.getFabric(), is("HB"));
    }

    @Test
    void findAll() {
        List<GenaBukinSocks> result = repository.findAll();

        assertThat("Wrong result size", result, hasSize(3));
    }

    @Test
    void findAllWithCriteriaDto() {

        GenaBukinSocksCriteriaDto criteriaDto = new GenaBukinSocksCriteriaDto("JustSocks", 61, 59,
                4, 1002, 1003, 13, PowerSmell.DEADLY, "HB");

        List<GenaBukinSocks> result = repository.findAll(criteriaDto);

        assertThat("Wrong result size", result, hasSize(1));
        assertThat("Wrong result", result, allOf(
                hasItem(allOf(
                        hasProperty("id", is(1001)),
                        hasProperty("name", is("JustSocks")),
                        hasProperty("priceInCents", is(60)),
                        hasProperty("guarantyInMonths", is(4)),
                        hasProperty("country", is(allOf(hasProperty("id", is(1002))))),
                        hasProperty("producer", is(allOf(hasProperty("id", is(1003))))),
                        hasProperty("holesQuantity", is(13)),
                        hasProperty("powerSmell", is(PowerSmell.DEADLY)),
                        hasProperty("fabric", is("HB"))
                ))));

    }

    @Disabled
    @ExpectedDatabase(table = "socks", value = "classpath:dbunit/expectation/socks-update.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    @Test
    void update() {

        repository.update(new GenaBukinSocks()
                .setId(1003)
                .setName("TrueJustSocks")
                .setPriceInCents(820)
                .setProductionDate(Instant.parse("2008-11-24T11:10:00Z"))
                .setGuarantyInMonths(9)
                .setCountry(new Country()
                        .setId(1002))
                .setProducer(new Producer()
                        .setId(1003))
                .setPowerSmell(PowerSmell.STRONGLY)
                .setFabric("BB")
                .setHolesQuantity(27));

    }

    @Disabled
    @ExpectedDatabase(table = "socks", value = "classpath:dbunit/expectation/socks-delete.xml",
            assertionMode = DatabaseAssertionMode.NON_STRICT_UNORDERED)
    @Test
    void deleteByID() {

        repository.deleteById(1003);

    }

    @Test
    void findByName() {
        GenaBukinSocks socks = repository.findByName("SimpleSocks", 1001).get();

        assertThat("Wrong price", socks.getPriceInCents(), is(160));
        assertThat("Wrong date", socks.getProductionDate().toString(), is("2017-08-24T11:10:00Z"));
        assertThat("Wrong guaranty", socks.getGuarantyInMonths(), is(7));
        assertThat("Wrong country", socks.getCountry().getId(), is(1001));
        assertThat("Wrong holes", socks.getHolesQuantity(), is(18));
        assertThat("Wrong smell", socks.getPowerSmell(), is(PowerSmell.DEADLY));
        assertThat("Wrong fabric", socks.getFabric(), is("HB"));
    }
}